from micp_planner.common.configuration_builder import ConfigurationBuilder
from micp_planner.common.utils import load_scenario_and_planning_problem, get_scenarios_location_from_directory, \
    write_result_file
from micp_planner.planners.planner_utils import get_planner, run_planner_with_context


def main():
    # Specify the planner type, possible options include 'shc', 'ssc', 'msc', or 'lex'
    use_planner = 'lex'

    # Define the maximum number of scenarios to consider
    max_nof_scenarios = 10000

    # If True, suppress all print outputs
    suppress_prints = False

    # List of rules to be used for multi-objective planning
    rulebook_multi = ['R_G1_SAFE_DISTANCE',
                      'R_G2_ABRUPT_BRAKING',
                      'R_G3_MAXIMUM_SPEED_LIMIT',
                      'R_G4_TRAFFIC_FLOW',
                      'R_M1_REACH_GOAL']

    # Rule to be used for single-objective planning
    rulebook_single = ['R_G1_G2_G3_G4_M1']

    # Create a planner instance with the specified planner type and rulebooks
    config = ConfigurationBuilder.build_configuration('')
    planner = get_planner(use_planner, rulebook_multi, rulebook_single, config)

    # Retrieve the locations of scenario files from the given directory
    scenario_locations = get_scenarios_location_from_directory(config.general.path_scenarios, max_nof_scenarios)

    # Initialize lists for storing results and failed scenarios
    results = []
    failed = []

    # Iterate over each scenario file
    for i, scenario_location in enumerate(scenario_locations):
        print(f'----------------- Scenario {i+1}/{len(scenario_locations)} -----------------')

        # Load scenario, planning problem for each scenario file (config is ignored)
        scenario, planning_problem, _ = load_scenario_and_planning_problem(scenario_location)

        try:
            # Run the planner with the given scenario and planning problem, and store the result
            results.append(run_planner_with_context(planner, scenario, planning_problem, config, suppress_prints,
                                                    make_plots=False))
        except Exception as e:
            # If there's an error, record the scenario id and error message
            failed.append([scenario.scenario_id, str(e)])

        # Write the results and failed scenarios to a file
        write_result_file(results, failed, config)


if __name__ == "__main__":
    main()
