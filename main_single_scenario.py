from micp_planner.common.utils import load_scenario_and_planning_problem
from micp_planner.planners.planner_utils import get_planner
from micp_planner.planners.run_planner import run_planner


def main():
    # Set the planner type, one of 'shc', 'ssc', 'msc', or 'lex'
    use_planner = 'lex'

    # Specify the scenario to use by index from the scenarios dictionary
    use_scenario = 0

    # Define a dictionary to store different scenario options
    scenarios = {0: "DEU_Guetersloh-6_1_T-1",
                 1: "DEU_Flensburg-98_1_T-1",
                 2: "PRI_Barceloneta-2_1_T-1"}

    # Load the selected scenario and the corresponding planning problem from an XML file
    scenario, planning_problem, config = load_scenario_and_planning_problem(scenarios[use_scenario] + ".xml")

    # Define a list of rules to be used for multi-objective planning
    rulebook_multi = ['R_G1_SAFE_DISTANCE',
                      'R_G2_ABRUPT_BRAKING',
                      'R_G3_MAXIMUM_SPEED_LIMIT',
                      'R_G4_TRAFFIC_FLOW',
                      'R_M1_REACH_GOAL']

    # Define a rule to be used for single-objective planning
    rulebook_single = ['R_G1_G2_G3_G4_M1']

    # Get an instance of the specified planner with the rulebooks and the configuration
    planner = get_planner(use_planner, rulebook_multi, rulebook_single, config)

    # Run the planner on the scenario and the planning problem with the provided configuration
    run_planner(planner, scenario, planning_problem, config)


if __name__ == "__main__":
    main()
