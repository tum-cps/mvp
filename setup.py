from os import path
from setuptools import setup, find_packages

this_directory = path.abspath(path.dirname(__file__))
with open("README.md", "r") as fh:
    long_description = fh.read()

with open(path.join(this_directory, 'requirements.txt'), encoding='utf-8') as f:
    required = f.read().splitlines()

setup(
    name="micp_mvp",
    version="2023.1",
    description="Lexicographic Mixed-Integer Motion Planning with STL Constraints",
    long_description=long_description,
    url="https://gitlab.lrz.de/tum-cps/mvp",
    packages=find_packages(),
    python_requires='>=3.9',
    install_requires=required,
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux"
    ]
)
