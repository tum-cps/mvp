import os
from typing import List

from tabulate import tabulate

from micp_planner.common.configuration_builder import ConfigurationBuilder
from micp_planner.common.utils import get_scenarios_location_from_directory, load_scenario_and_planning_problem
from micp_planner.planners.planner_utils import get_planner
from micp_planner.planners.run_planner import compare_planners


def main():
    # Define the maximum number of scenarios to consider
    max_nof_scenarios = 10000

    # List of rules to be used for multi-objective planning
    rulebook_multi = ['R_G1_SAFE_DISTANCE',
                      'R_G2_ABRUPT_BRAKING',
                      'R_G3_MAXIMUM_SPEED_LIMIT',
                      'R_G4_TRAFFIC_FLOW',
                      'R_M1_REACH_GOAL']

    # Rule to be used for single-objective planning
    rulebook_single = ['R_G1_G2_G3_G4_M1']

    # Create the planner instances
    config = ConfigurationBuilder.build_configuration('')
    planner_shc = get_planner('shc', rulebook_multi, rulebook_single, config)
    planner_ssc = get_planner('ssc', rulebook_multi, rulebook_single, config)
    planner_msc = get_planner('msc', rulebook_multi, rulebook_single, config)
    planner_lex = get_planner('lex', rulebook_multi, rulebook_single, config)

    planners = [planner_lex, planner_shc, planner_ssc, planner_msc]

    # Retrieve the locations of scenario files from the given directory
    scenario_locations = get_scenarios_location_from_directory(config.general.path_scenarios, max_nof_scenarios)

    # Create output directory
    evaluation_dir = config.general.path_output + 'evaluation'
    os.makedirs(evaluation_dir, exist_ok=True)

    # --------------------------------------------------------------------------------
    # Save solver information
    # --------------------------------------------------------------------------------
    # Define the output path and table headers
    output_path = os.path.join(evaluation_dir, 'details_solvers.txt')
    headers = ['Planner', 'Nof Var.', 'Nof Int. Var.', 'Nof Constr.', 'Nof z Constr.']

    # Generate solvers' statistics for each planner
    solvers_info = [[planner.name] + planner.solvers[-1].getStatistics() for planner in planners]

    # Write the table into the file
    with open(output_path, 'w+') as file:
        file.write(tabulate(solvers_info, headers=headers))

    # --------------------------------------------------------------------------------
    # Plan with all planners
    # --------------------------------------------------------------------------------
    # Initialize results and failed lists
    results = {pl.name: [] for pl in planners}
    failed = []

    # Process each scenario
    for i, scenario_location in enumerate(scenario_locations):
        print(f'----------------- Scenario {i + 1}/{len(scenario_locations)} -----------------')

        # Load scenario and planning problem for each scenario file
        scenario, planning_problem, _ = load_scenario_and_planning_problem(scenario_location)
        try:
            # Append comparison results to respective planner in results dict
            for comp_result in compare_planners(planner_lex, [planner_shc, planner_ssc, planner_msc], scenario,
                                                planning_problem, config):
                results[comp_result['planner']].append(comp_result)
        except Exception as e:
            failed.append([scenario.scenario_id, e])

        # Write planner output files
        headers = ['Scenario Id', 'Converged', 'Model time [s]', 'm']
        keys = ['scenario_id', 'converged', 'model_time', 'm_score']  # The keys in your result dictionary
        for planner_name, result in results.items():
            with open(os.path.join(evaluation_dir, f'eval_{planner_name}.txt'), 'w+') as file:
                file.write(tabulate([[res[key] for key in keys] for res in result], headers=headers, floatfmt=".16f"))

        # Write failed scenarios into file
        with open(os.path.join(evaluation_dir, 'failed_scenarios.txt'), 'w+') as file:
            file.write(tabulate(failed))

    # -------------------------------------------
    # Make evaluation of results
    # -------------------------------------------
    headers = ['Planner', 'Converged scenarios', 'Scenarios m>0', 'm', 'Solver time']
    evaluations = []

    # Loop over all results
    for planner_name, result in results.items():
        # Compute results directly inside append
        evaluations.append([
            planner_name,
            sum(res['converged'] for res in result),
            # 'converged' should be a boolean, summing up gives the number of True
            sum(res['m_score'] > 0 for res in result if res['m_score'] is not None),  # Scenarios with m_score > 0
            min_avr_max([res['m_score'] for res in result if res['m_score'] is not None]) if any(
                res['m_score'] is not None for res in result) else '-',  # m values
            min_avr_max([res['model_time'] for res in result if res['model_time'] is not None]) if any(
                res['model_time'] is not None for res in result) else '-'  # Solver times
        ])

    # Write into file
    with open(os.path.join(evaluation_dir, 'evaluation.txt'), 'w+') as file:
        file.write(tabulate(evaluations, headers=headers, floatfmt=".4f"))


def min_avr_max(v: List, prec: int = 2):
    return round(min(v), prec), round(sum(v) / len(v), prec), round(max(v), prec)


if __name__ == "__main__":
    main()
