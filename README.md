# Lexicographic Mixed-Integer Motion Planning with STL Constraints

---
A micp-based motion planner utilizing prioritized STL specifications.

---
### Installation
1. Clone this repository & create a new conda environment `conda create -n commonroad-py39 python=3.9`
2. Install the package:
   - Run the pip setup in the root folder: `pip install .`
   - **Or** install the dependencies with `pip install -r requirements.txt` and add the root folder to the python path of your interpreter

**Note**: A [Gurobi](https://www.gurobi.com/) license is required to execute the optimization.

---
   
### Getting started
* Execute a single scenario: `python main_single_scenario.py`
* Execute multiple scenarios: `python main_multiple_scenarios.py`
* Execute the evaluation: `python evaluation.py`

---
### Citation
If you use our planner for academic work, we highly encourage you to cite our paper:

```text
@InProceedings{Halder2023,
  author    = {Halder, Patrick and Christ, Fabian and Althoff, Matthias},
  booktitle = {IEEE 26th International Conference on Intelligent Transportation Systems (ITSC)},
  title     = {Lexicographic Mixed-Integer Motion Planning with STL Constraints},
  year      = {2023},
}
```
---