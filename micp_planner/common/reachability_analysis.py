import os
from typing import List, Tuple, Union

import numpy.typing as npt
import portion as Int
from commonroad.planning.planning_problem import PlanningProblem
from commonroad.scenario.scenario import Scenario
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from commonroad_reach.data_structure.reach.reach_polygon import ReachPolygon
from commonroad_reach.utility import reach_operation

from micp_planner.common.configuration import Configuration
from micp_planner.common.prediction import Prediction
from micp_planner.common.vehicle_model import VehicleModel
from micp_planner.common.visualization import plotReachablePositionIntervals, \
    plotScenarioWithReachablePosIntervals, visualizeReachableSet


def getReachableSet(predictions: List[Prediction], x_0: npt.NDArray, scenario: Scenario,
                    planning_problem: PlanningProblem, ccosy: CurvilinearCoordinateSystem,
                    goal_s_max: float, vehicle_model: VehicleModel,
                    config: Configuration) -> Tuple[List[List[ReachPolygon]], bool]:

    # Parameters
    K = config.planning.planning_horizon
    dt = config.planning.dt
    ego_length = config.vehicle.length
    v_long_min = config.vehicle.v_min
    v_long_max = config.vehicle.v_max
    a_long_min = config.vehicle.a_min
    a_long_max = config.vehicle.a_max
    epsilon = config.planning.reach_init_uncertainty

    # Get obstacle position intervals
    obst_pos_intervals = getObstaclePositionIntervalsInCorridor(K, predictions, ego_length)

    # Create initial polygon (we extend the in pos s and v direction by epsilon)
    tuple_vertices = (x_0[0], x_0[1], x_0[0]+epsilon, x_0[1]+epsilon)
    initial_polygon = ReachPolygon.from_rectangle_vertices(*tuple_vertices)

    # Create zero state polygon
    polygon_zero_state = reach_operation.create_zero_state_polygon(dt, a_long_min, a_long_max)

    # Reachable ego positions (when no obstacles are considered).
    # This info is required to remove non-reachable position interval snippets
    lower_ego_pos_bound = vehicle_model.getLowerBound(x_0, K)[0, :]
    upper_ego_pos_bound = vehicle_model.getUpperBound(x_0, K)[0, :]

    # Propagate polygons for each time step
    polygons = [[initial_polygon]]
    for time_step in range(K):
        pos_intervals = obst_pos_intervals[time_step + 1]
        prop_polygons_at_next_time_step = []
        for polygon in polygons[time_step]:
            # Propagate polygon
            polygon_propagated = reach_operation.propagate_polygon(polygon, polygon_zero_state, dt, v_long_min, v_long_max)

            # Intersect with s_min=0 and s_max=upper goal position
            polygon_propagated = intersectHalfspace(polygon_propagated, 0.0, 's_min')
            polygon_propagated = intersectHalfspace(polygon_propagated, goal_s_max, 's_max')

            # Intersect with obstacle position intervals
            polygons_propagated = [polygon_propagated]
            for pos_interval in pos_intervals:
                obs_s_min, obs_s_max = pos_interval.lower, pos_interval.upper

                intersected_polygons = []
                for polygon_prop in polygons_propagated:
                    # Intersect with the upper obstacle bound and remove possible snippets caused by over-approximation
                    right_polygon = intersectHalfspace(polygon_prop, obs_s_max, 's_min')
                    right_polygon = intersectHalfspace(right_polygon, lower_ego_pos_bound[time_step + 1], 's_min')
                    right_polygon = intersectHalfspace(right_polygon, upper_ego_pos_bound[time_step + 1], 's_max')

                    # Intersect with the lower obstacle bound and remove possible snippets caused by over-approximation
                    left_polygon = intersectHalfspace(polygon_prop, obs_s_min, 's_max')
                    left_polygon = intersectHalfspace(left_polygon, lower_ego_pos_bound[time_step + 1], 's_min')
                    left_polygon = intersectHalfspace(left_polygon, upper_ego_pos_bound[time_step + 1], 's_max')

                    intersected_polygons.extend([right_polygon, left_polygon])
                polygons_propagated = intersected_polygons

            # Remove empty polygons (= Nones)
            prop_polygons_at_next_time_step.extend([p for p in polygons_propagated if p is not None])
        polygons.append(prop_polygons_at_next_time_step)

    # Check if reachable set vanishes over time
    reachable_sets_vanish = checkIfReachableSetVanishes(polygons)

    # Do some plotting
    plot_reachable_sets = False
    plot_reachable_position_intervals = False
    plot_scenario_with_reachable_position_intervals = False

    if plot_reachable_sets:
        visualizeReachableSet(polygons, os.getcwd() + "/reach_plots", config, overlap=True, create_video=True)
    if plot_reachable_position_intervals:
        plotReachablePositionIntervals(K, polygons, vehicle_model, x_0)
    if plot_scenario_with_reachable_position_intervals:
        plotScenarioWithReachablePosIntervals(polygons, planning_problem, scenario, x_0, ccosy, dt, "/reach_plots",
                                              create_video=True)
    return polygons, reachable_sets_vanish


def intersectHalfspace(polygon: ReachPolygon, threshold: float, case: str) -> Union[ReachPolygon, None]:
    if polygon is None:
        return None

    if case == "s_min":
        return polygon.intersect_halfspace(-1, 0, -threshold)
    elif case == "s_max":
        return polygon.intersect_halfspace(1, 0, threshold)
    elif case == "v_min":
        return polygon.intersect_halfspace(0, -1, -threshold)
    elif case == "v_max":
        return polygon.intersect_halfspace(0, 1, threshold)
    else:
        raise Warning('This halfspace intersection case is not implemented!')


def getObstaclePositionIntervalsInCorridor(K: int, predictions: List[Prediction], ego_length: float):
    obst_pos_intervals = []
    for time_step in range(K+1):
        obst_intervals_at_time = []
        for prediction in predictions:
            # 1. Check if obstacles intersect with corridor
            corridor_overlap = prediction.corridor_overlap[time_step]
            if corridor_overlap is not None and corridor_overlap >= 0.0:
                # 2. Determine obstacle position interval [s_min, s_max] +- ego_length/2 +- offset
                obst_intervals_at_time.append(Int.closed(prediction.s_min[time_step] - ego_length / 2,
                                                         prediction.s_max[time_step] + ego_length / 2))
        obst_pos_intervals.append(obst_intervals_at_time)
    return obst_pos_intervals


def checkIfReachableSetVanishes(polygons: List[List[ReachPolygon]]) -> bool:
    return len(polygons[-1]) == 0
