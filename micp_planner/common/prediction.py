import logging
from typing import Tuple, List

import numpy as np
import numpy.typing as npt
from commonroad.common.util import Interval
from commonroad.scenario.obstacle import Obstacle, ObstacleType
from commonroad.scenario.scenario import Scenario
from commonroad.scenario.traffic_sign import SupportedTrafficSignCountry
from commonroad.scenario.traffic_sign_interpreter import TrafficSigInterpreter
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem

from micp_planner.common.configuration import Configuration
from micp_planner.common.mission import Mission
from micp_planner.common.utils import getObstacleState, createObstacleShape, getRelativeObstacleOrientation, \
    getSpeedLimit


def getPredictionsFromScenario(scenario: Scenario, ccosy: CurvilinearCoordinateSystem, mission: Mission,
                               current_time_step: int,
                               x_0: npt.NDArray, config: Configuration):
    supported_obstacle_types = {ObstacleType.CAR,
                                ObstacleType.TRUCK,
                                ObstacleType.BUS,
                                ObstacleType.PRIORITY_VEHICLE,
                                ObstacleType.TRAIN,
                                ObstacleType.MOTORCYCLE,
                                ObstacleType.TAXI,
                                ObstacleType.BICYCLE}

    traffic_sign_interpreter = TrafficSigInterpreter(SupportedTrafficSignCountry(scenario.scenario_id.country_id),
                                                     scenario.lanelet_network)
    predictions = []
    obstacles = {'used': [], 'ignored': []}

    for obstacle in scenario.obstacles:
        if obstacle.obstacle_type in supported_obstacle_types:
            new_prediction = Prediction(obstacle, scenario, ccosy, mission, current_time_step, traffic_sign_interpreter,
                                        config)
            # Only add prediction if all trajectory states (resp. for complete time horizon) are available
            if new_prediction.all_trajectory_states_available:
                predictions.append(new_prediction)
            else:
                obstacles['ignored'].append(new_prediction.cr_obstacle_id)
                logging.warning(f'Not all trajectory states available for obstacle {obstacle.obstacle_id}.'
                                f' It is not considered further!')
        else:
            obstacles['ignored'].append(obstacle.obstacle_id)
            logging.warning(f'Type {obstacle.obstacle_type} of obstacle {obstacle.obstacle_id} is not supported.'
                            f' It is not considered further!')

    # sort predictions with respect to their distance to ego position
    x_0_cartesian = ccosy.convert_to_cartesian_coords(x_0[0], 0.0)
    predictions = sorted(predictions, key=lambda obj: obj.euclidianDistance(x_0_cartesian))

    # extract obstacles to be considered (solver excepts predefined maximum number of obstacles)
    predictions = predictions[:config.planning.nof_considered_obstacles]
    for prediction in predictions:
        obstacles['used'].append(prediction.cr_obstacle_id)

    # overwrite obstacle ids
    for obstacle_id, prediction in enumerate(predictions):
        prediction.overwriteObstacleId(obstacle_id)

    return predictions, obstacles


class Prediction:
    def __init__(self, obstacle: Obstacle, scenario: Scenario, ccosy: CurvilinearCoordinateSystem, mission: Mission,
                 current_time_step: int, traffic_sign_interpreter: TrafficSigInterpreter, config: Configuration):
        self.cr_obstacle = obstacle
        self.cr_obstacle_id = obstacle.obstacle_id
        self.obstacle_id = obstacle.obstacle_id  # This will be overwritten

        self.prediction_horizon = config.planning.planning_horizon + 1
        self.current_time_step = current_time_step
        self.time_step_increment = int(config.planning.dt / scenario.dt)
        self.final_time_step = current_time_step + self.prediction_horizon

        self.scenario_dt = scenario.dt

        self.mission = mission

        # Trajectory states
        self.position = []
        self.orientation = []
        self.velocity = []
        self.acceleration = []

        # Derived states
        self.s_min = []
        self.s_max = []
        self.d_min = []
        self.d_max = []
        self.theta_rel = []
        self.corridor_overlap = []
        self.lane_overlap = []
        self.corridor_boundary_overlap = []
        self.lane_boundary_overlap = []
        self.intersecting_lanes = []
        self.safe_dist_params = []
        self.safe_dist_params_multi = []
        self.v_max_2 = []

        self.all_trajectory_states_available = self.setTrajectoryValues(obstacle,
                                                                        config.planning.extrapolate_obstacle_prediction)

        # Only calculate the derived states from trajectory, if all trajectory states are available
        if self.all_trajectory_states_available:
            self.setDerivedStates(obstacle, scenario, ccosy, traffic_sign_interpreter, config)

    def setTrajectoryValues(self, obstacle: Obstacle, extrapolate: bool = False) -> bool:
        for step in range(self.prediction_horizon):
            time_step = self.current_time_step + step * self.time_step_increment
            obstacle_state = getObstacleState(obstacle, time_step, extrapolate=extrapolate,
                                              scenario_dt=self.scenario_dt)

            # If there exists an obstacle state for the current time steps, and it has all necessary attributes
            if obstacle_state is not None \
                    and hasattr(obstacle_state, 'position') \
                    and hasattr(obstacle_state, 'orientation') \
                    and hasattr(obstacle_state, 'velocity') \
                    and hasattr(obstacle_state, 'acceleration'):

                self.position.append(obstacle_state.position)
                self.orientation.append(obstacle_state.orientation)
                self.velocity.append(obstacle_state.velocity)
                self.acceleration.append(obstacle_state.acceleration)

            else:
                # We could not get the states for all time steps and thus return False
                return False

        # We could get the states for all time steps
        return True

    def setDerivedStates(self, obstacle: Obstacle, scenario: Scenario, ccosy: CurvilinearCoordinateSystem,
                         traffic_sign_interpreter: TrafficSigInterpreter, config: Configuration):
        for time_step in range(self.prediction_horizon):
            # --------- Reference Path Projections ------------------------
            s_min_k, s_max_k, d_min_k, d_max_k, theta_rel_k = getReferencePathProjections(obstacle,
                                                                                          self.position[time_step],
                                                                                          self.orientation[time_step],
                                                                                          ccosy)
            self.s_min.append(s_min_k)
            self.s_max.append(s_max_k)
            self.d_min.append(d_min_k)
            self.d_max.append(d_max_k)
            self.theta_rel.append(theta_rel_k)

            # --------- Corridor overlap ----------------------------------
            corridor_overlap_k = getCorridorOverlap(self.d_min[time_step], self.d_max[time_step], config)
            self.corridor_overlap.append(corridor_overlap_k)

            lane_overlap_k = getLaneOverlap(obstacle, self.position[time_step], self.orientation[time_step],
                                            self.mission.left_boundary, self.mission.right_boundary)
            self.lane_overlap.append(lane_overlap_k)

            # --------- Corridor boundary overlap -------------------------
            corridor_boundary_overlap_k = getCorridorBoundaryOverlap(self.d_min[time_step], self.d_max[time_step],
                                                                     config)
            self.corridor_boundary_overlap.append(corridor_boundary_overlap_k)

            lane_boundary_overlap_k = getLaneBoundaryOverlap(obstacle, self.position[time_step],
                                                             self.orientation[time_step],
                                                             self.mission.left_boundary, self.mission.right_boundary)
            self.lane_boundary_overlap.append(lane_boundary_overlap_k)

            # --------- Lanelet Overlap -----------------------------------
            intersecting_lanes_k = getLaneletOverlap(scenario, self.position[time_step])
            self.intersecting_lanes.append(intersecting_lanes_k)

            # --------- Parameters of the line equation for safe distance approximation --------------
            safe_dist_params_k = getParamsOfSafeDistApproximation(self.velocity[time_step], config)
            safe_dist_params_multi_k = getParamsOfSafeDistApproximationMulti(self.velocity[time_step], config)
            self.safe_dist_params.append(safe_dist_params_k)
            self.safe_dist_params_multi.append(safe_dist_params_multi_k)

            # --------- v_max_2 (see rule R_G4) ---------------------------
            v_max_2_k = getV2(self.intersecting_lanes[time_step], traffic_sign_interpreter, config)
            self.v_max_2.append(v_max_2_k)

    def overwriteObstacleId(self, obstacle_id: int):
        self.obstacle_id = obstacle_id

    def euclidianDistance(self, x: npt.NDArray) -> float:
        return np.linalg.norm(self.position[0] - x)


def getReferencePathProjections(obstacle: Obstacle, position: npt.NDArray, orientation: float,
                                ccosy: CurvilinearCoordinateSystem) -> Tuple[float, float, float, float, float]:
    # Default values
    s_min, s_max = -100.0, -100.0
    d_min, d_max = -100.0, -100.0
    theta_rel = 0.0

    obstacle_occupancy = createObstacleShape(obstacle, position, orientation)
    transformed_set, _ = ccosy.convert_list_of_polygons_to_curvilinear_coords_and_rasterize(
        [obstacle_occupancy.vertices], [0], 1, 4)

    # Only if transformed data is available
    if len(transformed_set[0]) > 0:
        s_min = min([arr.tolist()[0] for arr in transformed_set[0][0]])
        s_max = max([arr.tolist()[0] for arr in transformed_set[0][0]])

        d_min = min([arr.tolist()[1] for arr in transformed_set[0][0]])
        d_max = max([arr.tolist()[1] for arr in transformed_set[0][0]])

        theta_rel = getRelativeObstacleOrientation((s_max + s_min) / 2, ccosy, orientation)

    return s_min, s_max, d_min, d_max, theta_rel


def get_ccosy_lateral_offset(obstacle: Obstacle, position: npt.NDArray, orientation: float,
                             ccosy: CurvilinearCoordinateSystem) -> Tuple[float, float, str]:
    # Default values
    d_min, d_max = -100.0, -100.0

    obstacle_occupancy = createObstacleShape(obstacle, position, orientation)
    try:
        transformed_set, _ = ccosy.convert_list_of_polygons_to_curvilinear_coords_and_rasterize(
            [obstacle_occupancy.vertices], [0], 1, 4)

        # Only if transformed data is available
        if len(transformed_set[0]) > 0:
            d_min = min([arr.tolist()[1] for arr in transformed_set[0][0]])
            d_max = max([arr.tolist()[1] for arr in transformed_set[0][0]])

    except:
        pass

    if d_min < 0 and d_max < 0:
        relation = 'is_right'
    elif d_min > 0 and d_max > 0:
        relation = 'is_left'
    else:
        relation = 'is_on'

    return d_min, d_max, relation


def getCorridorOverlap(d_min: float, d_max: float, config: Configuration) -> float:
    corridor_width = config.rulebook.d_corr
    corridor_d_interval = Interval(-corridor_width / 2, corridor_width / 2)
    obstacle_d_interval = Interval(d_min, d_max)

    if corridor_d_interval.overlaps(obstacle_d_interval):
        overlap = corridor_d_interval.intersection(obstacle_d_interval).length
    else:
        overlap = -min(abs(obstacle_d_interval.end - corridor_d_interval.start),
                       abs(obstacle_d_interval.start - corridor_d_interval.end))

    return overlap


def getLaneOverlap(obstacle: Obstacle, position: npt.NDArray, orientation: float,
                   left_boundary: CurvilinearCoordinateSystem, right_boundary: CurvilinearCoordinateSystem) -> float:
    d_min_l, d_max_l, relation_l = get_ccosy_lateral_offset(obstacle, position, orientation, left_boundary)
    d_min_r, d_max_r, relation_r = get_ccosy_lateral_offset(obstacle, position, orientation, right_boundary)

    overlap = 0.0
    # Obstacle is outside of corridor
    if relation_l == "is_left" or relation_r == "is_right":
        overlap = -min(abs(d_min_l), abs(d_max_l), abs(d_min_r), abs(d_max_r))
    # Obstacle is in corridor
    elif relation_l == "is_right" and relation_r == "is_left":
        overlap = min(max(abs(d_min_l), abs(d_max_l)), max(abs(d_min_r), abs(d_max_r)))
    # Obstacle occupies left boundary
    elif relation_l == "is_on":
        overlap = abs(d_min_l)
    # Obstacle occupies right boundary
    elif relation_r == "is_on":
        overlap = abs(d_max_r)

    return overlap


def getCorridorBoundaryOverlap(d_min: float, d_max: float, config: Configuration) -> bool:
    corridor_width = config.rulebook.d_corr

    if d_min <= corridor_width / 2 <= d_max or d_min <= -corridor_width / 2 <= d_max:
        return True
    else:
        return False


def getLaneBoundaryOverlap(obstacle: Obstacle, position: npt.NDArray, orientation: float,
                           left_boundary: CurvilinearCoordinateSystem,
                           right_boundary: CurvilinearCoordinateSystem) -> float:
    d_min_l, d_max_l, relation_l = get_ccosy_lateral_offset(obstacle, position, orientation, left_boundary)
    d_min_r, d_max_r, relation_r = get_ccosy_lateral_offset(obstacle, position, orientation, right_boundary)

    overlap = 0.0
    # Obstacle is inside or outside of corridor
    if relation_l == "is_left" or relation_r == "is_right" or (relation_l == "is_right" and relation_r == "is_left"):
        overlap = -min(abs(d_min_l), abs(d_max_l), abs(d_min_r), abs(d_max_r))
    # Obstacle occupies left boundary
    elif relation_l == "is_on":
        overlap = min(abs(d_min_l), abs(d_max_l))
    # Obstacle occupies right boundary
    elif relation_r == "is_on":
        overlap = min(abs(d_min_r), abs(d_max_r))

    return overlap


def getLaneletOverlap(scenario: Scenario, position: npt.NDArray) -> List:
    intersecting_lanes = scenario.lanelet_network.find_lanelet_by_position([position])[0]
    if len(intersecting_lanes) > 0:
        return intersecting_lanes
    else:
        return []


def getParamsOfSafeDistApproximation(obstacle_velocity: float, config: Configuration) -> dict:
    # Check if the min ego velocity is >= 0 -> Otherwise the approximation is not valid
    assert config.vehicle.v_min >= 0, "The minimum velocity of the ego is < 0. " \
                                      "The safe distance approximation does not hold for this case!"

    # Get parameters
    t_react = config.rulebook.t_react
    v_e_max = config.vehicle.v_max
    a_e_min = config.vehicle.a_min
    a_o_min = config.rulebook.a_min_obs

    v_o = obstacle_velocity

    # Parameters of the line equation y = mx + b
    m = v_e_max / (2 * abs(a_e_min)) + t_react
    b = v_o ** 2 / (-2 * abs(a_o_min))

    return {'m': m, 'b': b}


def safe_distance(obstacle_velocity: float, ego_velocity: float, config: Configuration) -> float:
    # Check if the min ego velocity is >= 0 -> Otherwise the approximation is not valid
    assert config.vehicle.v_min >= 0, "The minimum velocity of the ego is < 0. " \
                                      "The safe distance approximation does not hold for this case!"

    # Get parameters
    t_react = config.rulebook.t_react
    a_e_min = config.vehicle.a_min
    a_o_min = config.rulebook.a_min_obs
    v_o = obstacle_velocity
    return v_o ** 2 / (-2 * abs(a_o_min)) - ego_velocity ** 2 / (-2 * abs(a_e_min)) + t_react * ego_velocity


def getParamsOfSafeDistApproximationMulti(obstacle_velocity: float, config: Configuration) -> List[dict]:
    nof_subspaces = 2
    v_min = config.vehicle.v_min - 0.001
    v_max = config.vehicle.v_max + 0.001

    delta_v = (v_max - v_min) / nof_subspaces

    ranges = []
    for i in range(nof_subspaces):
        v0 = v_min + i * delta_v
        v1 = v_min + (i + 1) * delta_v
        ranges.append((v0, v1))

    # Calculate parameters for each range
    params = []
    for r in ranges:
        vmin, vmax = r

        # Calculate y values at the ends of the range
        y_min = safe_distance(obstacle_velocity, vmin, config)
        y_max = safe_distance(obstacle_velocity, vmax, config)

        # Calculate slope and intercept
        m = (y_max - y_min) / (vmax - vmin)
        b = y_min - m * vmin
        params.append({'m': m, 'b': b})
    return params


def getV2(occupied_lanes: List, traffic_sign_interpreter: TrafficSigInterpreter, config: Configuration) -> float:
    delta_fl = config.rulebook.delta_fl
    v_max_2 = delta_fl

    lane_speed_limit = getSpeedLimit(traffic_sign_interpreter, tuple(occupied_lanes))
    if lane_speed_limit is not None:
        v_max_2 = min(lane_speed_limit, delta_fl)

    return v_max_2
