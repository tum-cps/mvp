import logging
from copy import deepcopy
from typing import Tuple, List, Dict

import numpy as np
import numpy.typing as npt
from commonroad.geometry.shape import ShapeGroup, Shape
from commonroad.planning.planning_problem import PlanningProblem
from commonroad.scenario.scenario import Scenario
from commonroad.scenario.traffic_sign import SupportedTrafficSignCountry
from commonroad.scenario.traffic_sign_interpreter import TrafficSigInterpreter
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem

from micp_planner.common.configuration import Configuration
from micp_planner.common.utils import getSpeedLimit, getMaxVelocityBasedOnLateralAcceleration, getLaneBoundary


class Mission:
    def __init__(self, scenario: Scenario, planning_problem: PlanningProblem, ccosy: CurvilinearCoordinateSystem, route_lanelet_ids: List[int], config: Configuration):
        self.scenario = scenario
        self.planning_problem = planning_problem
        self.ccosy = ccosy
        self.route_lanelet_ids = route_lanelet_ids
        self.config = config

        self.initial_state = self.getInitialMissionState()

        self.left_boundary = getLaneBoundary(scenario.lanelet_network, route_lanelet_ids, 'left')
        self.right_boundary = getLaneBoundary(scenario.lanelet_network, route_lanelet_ids, 'right')

        self.global_goal_position_interval = self.getGoalPositionIntervalFromPlanningProblem()
        self.speed_limits = self.getSpeedLimitsAlongMission()
        self.global_kinematic_speed_limit_segments = self.calculateGlobalKinematicSpeedLimitSegments()

    def getGoalPositionIntervalFromPlanningProblem(self, eps=1e-3) -> Dict:
        default_goal_position_interval = self.config.rulebook.default_goal_position_interval

        # Check if goal area exists
        if len(self.planning_problem.goal.state_list) == 0 or not hasattr(self.planning_problem.goal.state_list[0], 'position'):
            ref_path_length = self.ccosy.length()
            s_min = ref_path_length - default_goal_position_interval
            s_max = ref_path_length
            logging.warning(f'Scenario {self.scenario.scenario_id} does not contain a goal area. '
                            f'Using default goal position interval [{s_min},{s_max}] instead.')

        else:
            min_goal_positions = []
            max_goal_positions = []
            for goal_state in self.planning_problem.goal.state_list:
                if isinstance(goal_state.position, ShapeGroup):
                    list_vertices = goal_state.position.shapes[0].vertices
                elif isinstance(goal_state.position, Shape):
                    list_vertices = goal_state.position.vertices

                transformed_set, _ = self.ccosy.convert_list_of_polygons_to_curvilinear_coords_and_rasterize([list_vertices], [0], 1, 4)
                min_goal_positions.append(min([arr.tolist()[0] for arr in transformed_set[0][0]]))
                max_goal_positions.append(max([arr.tolist()[0] for arr in transformed_set[0][0]]))

            s_min = min(min_goal_positions)
            s_max = max(max_goal_positions) - eps

        return {'s_min': s_min, 's_max': s_max}

    def getSpeedLimitsAlongMission(self) -> List[List]:
        fov_speed_limit = self.config.rulebook.fov_speed_limit
        braking_speed_limit = self.config.rulebook.braking_speed_limit
        road_condition_speed_limit = self.config.rulebook.road_condition_speed_limit
        desired_lane_velocity = self.config.rulebook.desired_lane_velocity

        lanelet_network = self.scenario.lanelet_network
        traffic_sign_interpreter = TrafficSigInterpreter(SupportedTrafficSignCountry(self.scenario.scenario_id.country_id), lanelet_network)

        speed_limits = []

        for lanelet_id in self.route_lanelet_ids:
            lanelet = lanelet_network.find_lanelet_by_id(lanelet_id)

            p_start = lanelet.center_vertices[0]
            pos_end = lanelet.center_vertices[-1]
            try:
                s_start, _ = self.ccosy.convert_to_curvilinear_coords(p_start[0], p_start[1])
            except ValueError:
                s_start = 0.0
                logging.warning(f'Could not project start point of lanelet {lanelet_id} onto ref path, setting s_start = 0.0')
            try:
                s_end, _ = self.ccosy.convert_to_curvilinear_coords(pos_end[0], pos_end[1])
            except ValueError:
                s_end = self.ccosy.length()
                logging.warning(f'Could not project end point of lanelet {lanelet_id} onto ref path, setting s_end = ccosy.length()')

            lane_speed_limit = getSpeedLimit(traffic_sign_interpreter, (lanelet_id,))

            if lane_speed_limit is None:
                lane_speed_limit = desired_lane_velocity

            speed_limit = min(lane_speed_limit, fov_speed_limit, braking_speed_limit, road_condition_speed_limit)

            # If list is still empty or the speed limit value deviates from the previously added speed limit
            if len(speed_limits) == 0 or speed_limit != speed_limits[-1][2]:
                # Add new speed limit [s_min, s_max, v_max]
                new_speed_limit = [s_start, s_end, speed_limit]
                speed_limits.append(new_speed_limit)
            else:
                # If speed limit does not deviate from previous one, just update s_max
                speed_limits[-1][1] = s_end

        return speed_limits

    def getInitialMissionState(self) -> npt.NDArray:
        return np.array([self.ccosy.convert_to_curvilinear_coords(self.planning_problem.initial_state.position[0],
                                                                  self.planning_problem.initial_state.position[1])[0],
                         self.planning_problem.initial_state.velocity])

    def get_global_goal_position_interval(self) -> Dict:
        return self.global_goal_position_interval

    def get_local_goal_position_interval(self, k_current: int) -> Dict:

        k_goal_global = self.planning_problem.goal.state_list[0].time_step.start
        k_goal_local = k_current + self.config.planning.planning_horizon

        if k_goal_local <= k_goal_global:
            s_min_local = self.global_goal_position_interval['s_min'] / k_goal_global * k_goal_local
            s_max_local = self.global_goal_position_interval['s_max'] / k_goal_global * k_goal_local
        else:
            s_min_local = self.global_goal_position_interval['s_min']
            s_max_local = self.global_goal_position_interval['s_max']

        return {'s_min': s_min_local, 's_max': s_max_local}

    def getMinMaxSpeed(self, x_0: npt.NDArray,  nof_consecutive_speed_limits: int, delta_v_min: float) \
            -> Tuple[List[List], List[List]]:

        next_speed_limits = []
        for i, speed_limit in enumerate(self.speed_limits):
            # If ego position is in position range of speed limit
            if speed_limit[0] <= x_0[0] < speed_limit[1]:
                for j in range(i, len(self.speed_limits)):
                    curr_speed_limit = self.speed_limits[j]
                    # If there are not enough speed limits yet
                    if len(next_speed_limits) < nof_consecutive_speed_limits:
                        next_speed_limits.append(curr_speed_limit)
                    else:
                        # If there are already enough sped limits in the list, change s_max and take min of v_max values
                        next_speed_limits[-1][1] = curr_speed_limit[1]
                        next_speed_limits[-1][2] = min(next_speed_limits[-1][2], curr_speed_limit[2])
                break

        next_min_speeds = deepcopy(next_speed_limits)
        for limit in next_min_speeds:
            limit[2] = min(0.0, limit[2] - delta_v_min)

        return next_speed_limits, next_min_speeds

    def calculateGlobalKinematicSpeedLimitSegments(self) -> List[List]:
        max_horizon_length = self.ccosy.length()
        min_horizon_length = min(max_horizon_length,
                                 (self.config.planning.planning_horizon * self.config.planning.dt * self.config.vehicle.v_max) *
                                 (1.0 + 1.0 / self.config.planning.nof_kinematic_speed_limit_segments))
        min_segment_length = min_horizon_length / self.config.planning.nof_kinematic_speed_limit_segments
        nof_segments = int(max_horizon_length / min_segment_length)
        segment_length = max_horizon_length / nof_segments
        global_kinematic_speed_limit_segments = []
        for i in range(nof_segments):
            s_min = i * segment_length
            s_max = s_min + segment_length
            v_max = getMaxVelocityBasedOnLateralAcceleration(s_min, s_max, self.ccosy, a_y_max=self.config.vehicle.a_y_max)
            global_kinematic_speed_limit_segments.append([s_min, s_max, v_max])
        return global_kinematic_speed_limit_segments

    def getKinematicSpeedLimitSegments(self, x_0: npt.NDArray, nof_kinematic_speed_limit_segments: int) -> List[List]:
        nof_global_kinematic_speed_limit_segments = len(self.global_kinematic_speed_limit_segments)
        start_station = x_0[0]

        # search for matching speed limit segment
        start_idx = 0
        for i, global_kinematic_speed_limit_segment in enumerate(self.global_kinematic_speed_limit_segments):
            if start_station > global_kinematic_speed_limit_segment[0]:
                start_idx = i
            else:
                break
        start_idx = min(start_idx, nof_global_kinematic_speed_limit_segments - nof_kinematic_speed_limit_segments)

        # extract kinematic speed limit segments
        kinematic_speed_limit_segments = []
        for i in range(nof_kinematic_speed_limit_segments):
            idx = start_idx + i
            kinematic_speed_limit_segments.append(self.global_kinematic_speed_limit_segments[idx])

        return kinematic_speed_limit_segments
