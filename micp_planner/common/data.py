from typing import List, Dict

import numpy.typing as npt
from commonroad.planning.planning_problem import PlanningProblem
from commonroad.scenario.scenario import Scenario
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from commonroad_reach.pycrreach import ReachPolygon

from micp_planner.common.configuration import Configuration
from micp_planner.common.mission import Mission
from micp_planner.common.prediction import getPredictionsFromScenario, Prediction
from micp_planner.common.reachability_analysis import getReachableSet
from micp_planner.common.vehicle_model import VehicleModel


class Data:
    def __init__(self, predictions: List[Prediction], obstacles: Dict, reachable_sets: List[List[ReachPolygon]],
                 reachable_sets_vanish: bool, max_speeds: List, min_speeds: List, kinematic_speed_limit_segments: List[List],
                 local_goal_position_interval: Dict):
        self.predictions = predictions
        self.obstacles = obstacles
        self.reachable_sets = reachable_sets
        self.reachable_sets_vanish = reachable_sets_vanish
        self.max_speeds = max_speeds
        self.min_speeds = min_speeds
        self.kinematic_speed_limit_segments = kinematic_speed_limit_segments
        self.local_goal_position_interval = local_goal_position_interval


class InputDataHandler:
    def __init__(self, scenario: Scenario, planning_problem: PlanningProblem, ccosy: CurvilinearCoordinateSystem,
                 route_lanelet_ids: List[int], config: Configuration):
        self.scenario = scenario
        self.planning_problem = planning_problem
        self.ccosy = ccosy
        self.route_lanelet_ids = route_lanelet_ids
        self.vehicle_model = VehicleModel(config.planning.dt, config.vehicle)
        self.config = config
        self.mission = Mission(scenario, planning_problem, ccosy, route_lanelet_ids, config)

    def get(self, current_time_step: int, x_0: npt.NDArray) -> Data:
        # Generate obstacle predictions
        predictions, obstacles = getPredictionsFromScenario(self.scenario, self.ccosy, self.mission, current_time_step, x_0,
                                                            self.config)

        # Generate reachable sets
        reachable_set, set_vanishes = getReachableSet(predictions, x_0, self.scenario, self.planning_problem,
                                                      self.ccosy, self.mission.get_global_goal_position_interval().get('s_max'),
                                                      self.vehicle_model, self.config)

        # Generate local goal position interval
        local_goal_position_interval = self.mission.get_local_goal_position_interval(current_time_step + self.config.planning.planning_horizon)

        # Generate maximum and minimum speeds
        max_speeds, min_speeds = self.mission.getMinMaxSpeed(x_0, self.config.planning.nof_max_speeds,
                                                             self.config.rulebook.delta_fl)

        # Generate kinematic speed limit segments
        kinematic_speed_limit_segments = self.mission.getKinematicSpeedLimitSegments(x_0, self.config.planning.nof_kinematic_speed_limit_segments)

        return Data(predictions, obstacles, reachable_set, set_vanishes, max_speeds, min_speeds,
                    kinematic_speed_limit_segments, local_goal_position_interval)
