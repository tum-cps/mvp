import logging
import math
import os
import sys
import warnings
from copy import deepcopy
from enum import Enum
from typing import Union, Tuple, List

import numpy as np
import numpy.typing as npt
from commonroad.common.file_reader import CommonRoadFileReader
from commonroad.geometry.shape import Shape
from commonroad.planning.planning_problem import PlanningProblem
from commonroad.scenario.lanelet import LaneletNetwork
from commonroad.scenario.obstacle import Obstacle, ObstacleRole
from commonroad.scenario.scenario import Scenario, Tag
from commonroad.scenario.traffic_sign_interpreter import TrafficSigInterpreter
from commonroad.scenario.trajectory import State
from commonroad_dc.geometry.util import chaikins_corner_cutting, resample_polyline
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from commonroad_route_planner.route_planner import RoutePlanner
from shapely.geometry import LineString
from tabulate import tabulate

from micp_planner.common.configuration import Configuration
from micp_planner.common.configuration_builder import ConfigurationBuilder


class ConstraintRelation(Enum):
    GREATER_THAN = 1.0   # ay -b >= 0
    LOWER_THAN = -1.0    # ay -b < 0


class SuppressPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')
        logging.getLogger().setLevel(logging.CRITICAL)
        warnings.filterwarnings('ignore')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout
        logging.getLogger().setLevel(logging.INFO)


def load_scenario_and_planning_problem(scenario_filename: str, idx_planning_problem: int = 0) -> Tuple[Scenario, PlanningProblem, Configuration]:
    config = ConfigurationBuilder.build_configuration(scenario_filename[:-4])
    scenario, planning_problem_set = CommonRoadFileReader(config.general.path_scenario).open()
    planning_problem = list(planning_problem_set.planning_problem_dict.values())[idx_planning_problem]
    return scenario, planning_problem, config


def get_scenarios_location_from_directory(scenarios_dir: str, max_nof_scenarios: int):
    locations = []
    abs_path = os.path.abspath(scenarios_dir)
    for filename in os.listdir(abs_path):
        if filename.startswith('C-'):  # Corresponds to multiple planning problems in one scenario file
            continue
        elif not filename.endswith('.xml'):
            continue
        locations.append(filename)

        if len(locations) == max_nof_scenarios:
            break
    return locations


def checkScenarioAndPlanningProblem(scenario: Scenario, planning_problem: PlanningProblem, config: Configuration):
    if (Tag.HIGHWAY or Tag.INTERSTATE) in scenario.tags:
        raise Warning('We do not support highway scenarios yet!')

    if planning_problem.initial_state.velocity > config.vehicle.v_max:
        raise Warning('The initial velocity of the planning problem is larger than ego v_max!')


def createReferencePath(planning_problem, scenario) -> Tuple[Union[CurvilinearCoordinateSystem, None], Union[List[int], None]]:
    # Instantiate a route planner with the scenario and the planning problem
    route_planner = RoutePlanner(scenario, planning_problem, backend=RoutePlanner.Backend.NETWORKX)
    route = route_planner.plan_routes().retrieve_best_route_by_orientation()

    if route:
        ref_path = route.reference_path

        # we apply chaikins corner cutting 10 times to smoothen the reference path
        for i in range(0, 10):
            ref_path = chaikins_corner_cutting(ref_path)
        ref_path = resample_polyline(ref_path, 2.0)

        # create curvilinear cosy
        ccosy = CurvilinearCoordinateSystem(ref_path, 25.0, 0.1)
        ccosy.compute_and_set_curvature()

        # check if ccosy is self intersecting
        proj_domain_border = np.asarray(ccosy.projection_domain())
        is_not_self_intersecting = LineString(proj_domain_border).is_simple

        if is_not_self_intersecting:
            return ccosy, route.list_ids_lanelets
    return None, None


def getLaneBoundary(lanelet_network: LaneletNetwork, lanelet_ids: List[int], boundary_side: str) -> CurvilinearCoordinateSystem:
    reference_path = None
    for idx, id_lanelet in enumerate(lanelet_ids):
        lanelet = lanelet_network.find_lanelet_by_id(id_lanelet)
        if boundary_side == 'left':
            vertices = lanelet.left_vertices
        elif boundary_side == 'right':
            vertices = lanelet.right_vertices
        else:
            raise ValueError

        for i in range(0, 10):
            vertices = chaikins_corner_cutting(vertices)
        vertices_resampled = resample_polyline(vertices, 1.0)

        if reference_path is None:
            reference_path = vertices_resampled
        else:
            reference_path = np.concatenate((reference_path, vertices_resampled), axis=0)

    reference_path = resample_polyline(reference_path, 2)
    return CurvilinearCoordinateSystem(reference_path, 25.0, 0.1)


def getOrientationAtStation(ccosy: CurvilinearCoordinateSystem, s: float) -> float:
    ego_tangent_vector = ccosy.tangent(s)
    # range [-pi, pi]
    orientation = math.atan2(ego_tangent_vector[1], ego_tangent_vector[0])
    # range [0, 2 pi]
    return orientation % (2 * math.pi)


def createObstacleShape(obstacle: Obstacle, position: npt.NDArray, orientation: float) -> Shape:
    return obstacle.obstacle_shape.rotate_translate_local(position, orientation)


def calculateAcceleration(obstacle: Obstacle, current_time_step: int, scenario_dt: float) -> float:
    initial_time_step = obstacle.initial_state.time_step
    final_time_step = obstacle.prediction.trajectory.final_state.time_step
    # get velocity at current time step
    if current_time_step == initial_time_step:
        velocity = obstacle.initial_state.velocity
    else:
        velocity = obstacle.prediction.trajectory.state_at_time_step(current_time_step).velocity
    # get velocity at next time step
    velocity_next = obstacle.prediction.trajectory.state_at_time_step(min(current_time_step + 1, final_time_step)).velocity
    return (velocity_next - velocity) / scenario_dt


def getObstacleState(obstacle: Obstacle, time_step=Union[int, None], extrapolate=False, scenario_dt=0.1) -> Union[State, None]:
    obst_state = None

    # Obstacle is dynamic
    if obstacle.obstacle_role == ObstacleRole.DYNAMIC:

        initial_time_step = obstacle.initial_state.time_step
        final_time_step = obstacle.prediction.trajectory.final_state.time_step

        # Time step is before initial state
        if time_step < initial_time_step and extrapolate:
            obst_state = deepcopy(obstacle.initial_state)
            obst_state.time_step = time_step
            ds = scenario_dt * (initial_time_step - time_step) * obst_state.velocity
            obst_state.position[0] -= math.cos(obst_state.orientation) * ds
            obst_state.position[1] -= math.sin(obst_state.orientation) * ds
            obst_state.acceleration = 0.0

        # Time step is initial state
        elif time_step == obstacle.initial_state.time_step:
            obst_state = obstacle.initial_state
            if not hasattr(obst_state, 'acceleration'):
                obst_state.acceleration = calculateAcceleration(obstacle, time_step, scenario_dt)

        # Time step is between initial and final time step
        elif initial_time_step < time_step <= final_time_step:
            obst_state = obstacle.prediction.trajectory.state_at_time_step(time_step)
            if not hasattr(obst_state, 'acceleration'):
                obst_state.acceleration = calculateAcceleration(obstacle, time_step, scenario_dt)

        elif time_step > final_time_step and extrapolate:
            obst_state = deepcopy(obstacle.prediction.trajectory.final_state)
            obst_state.time_step = time_step
            ds = scenario_dt * (time_step - final_time_step) * obst_state.velocity
            obst_state.position[0] += math.cos(obst_state.orientation) * ds
            obst_state.position[1] += math.sin(obst_state.orientation) * ds
            obst_state.acceleration = 0.0

    # Obstacle is static
    else:
        obst_state = obstacle.initial_state
        obst_state.velocity = 0.0
        obst_state.acceleration = 0.0

    return obst_state


def deltaAngles(angle1: float, angle2: float) -> float:
    return math.pi - abs(abs(angle1 - angle2) - math.pi)


def getRelativeObstacleOrientation(station: float, ccosy: CurvilinearCoordinateSystem,
                                   orientation_obst: float) -> float:
    orientation_ego = getOrientationAtStation(ccosy, station)
    return deltaAngles(orientation_ego, orientation_obst)


def getSpeedLimit(traffic_sign_interpreter: TrafficSigInterpreter, lanes: Tuple[int]):
    return traffic_sign_interpreter.speed_limit(frozenset(lanes))


def getMaxVelocityBasedOnLateralAcceleration(s_min: float, s_max: float, ccosy: CurvilinearCoordinateSystem,
                                             a_y_max: float) -> Union[float]:
    kappa_min, kappa_max = ccosy.curvature_range(max(0.0, s_min), min(ccosy.length(), s_max))
    abs_kappa_max = max(abs(kappa_min), abs(kappa_max), 1e-10)  # 1e-10 prevents division by zero
    return np.sqrt(a_y_max / abs_kappa_max)


def write_result_file(results: List, failed: List, config: Configuration, floatfmt=".6f"):

    # Header for results table
    rho_headers = [f'rho* {r} [-]' for r in range(len(config.rulebook.hierarchy))]
    headers = ['scenario ID', 'pl. time [s]', 'mod. time [s]'] + rho_headers + ['q* [-]']

    # Preprocess the data
    sorted_results = {0: [], 1: [], 2: [], 3: [], 4: [], 5: []}
    for result in results:
        # Get number of negative robustness values
        nof_neg = sum(1 for num in result['obj_opt'] if num < 0)
        sorted_results[nof_neg].append(result)

    with open(config.general.path_output + '/results_sorted.txt', 'w+') as file:
        for i in range (5,-1,-1):
            file.write(f'Nof violated rules: {i} \n')
            data = [[entry['scenario_id'], entry['planning_time'], entry['model_time'], *entry['obj_opt']] for entry in sorted_results[i]]
            file.write(tabulate(data, headers=headers, floatfmt=floatfmt))
            file.write('\n\n\n')
        file.write('\n\n\nFailed scenarios:\n')
        file.write(tabulate(failed))
