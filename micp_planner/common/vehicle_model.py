from typing import Tuple

import numpy as np
import numpy.typing as npt

from micp_planner.common.configuration import VehicleConfiguration


class VehicleModel:
    def __init__(self, dt: float, config: VehicleConfiguration):
        self._dt = dt
        self.vehicle_params = config

        self.A = np.array([[1, dt],
                           [0, 1]])
        self.B = np.array([[(dt ** 2) / 2],
                           [dt]])
        self.C = np.array([[1, 0],
                           [0, 1]])
        self.D = np.array([[0],
                           [0]])

        self.n = self.A.shape[1]
        self.m = self.B.shape[1]

    def getLowerBound(self, x_0: npt.NDArray, time_steps: int) -> npt.NDArray:
        lower_bound = np.empty((x_0.shape[0], time_steps+1))
        lower_bound[:, 0] = x_0

        for k in range(time_steps):
            lower_bound[:, k+1] = self._propagate_lb(lower_bound[:, k])

        return lower_bound

    def getUpperBound(self, x_0: npt.NDArray, time_steps: int) -> npt.NDArray:
        upper_bound = np.empty((x_0.shape[0], time_steps+1))
        upper_bound[:, 0] = x_0

        for k in range(time_steps):
            upper_bound[:, k+1] = self._propagate_ub(upper_bound[:, k])

        return upper_bound

    def _propagate_lb(self, state: np.ndarray) -> Tuple[float, float]:
        # Lower bound
        x_prime = self.A @ state + self.B @ np.array([self.vehicle_params.a_min])
        # If v_min is reached
        if x_prime[1] < self.vehicle_params.v_min:
            x_prime = np.array([[state[0] + self.vehicle_params.v_min * self._dt],
                                [self.vehicle_params.v_min]]).flatten()
        return x_prime

    def _propagate_ub(self, state: np.ndarray) -> Tuple[float, float]:
        # Upper bound
        x_prime = self.A @ state + self.B @ np.array([self.vehicle_params.a_max])
        # If v_max is reached
        if x_prime[1] > self.vehicle_params.v_max:
            x_prime = np.array([[state[0] + self.vehicle_params.v_max * self._dt],
                                [self.vehicle_params.v_max]]).flatten()
        return x_prime
