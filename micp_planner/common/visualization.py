import os
from copy import deepcopy
from typing import List, Dict

import matplotlib as mpl
import numpy as np
import numpy.typing as npt
from commonroad.geometry.shape import Rectangle
from commonroad.planning.planning_problem import PlanningProblem
from commonroad.prediction.prediction import TrajectoryPrediction
from commonroad.scenario.obstacle import ObstacleType, DynamicObstacle
from commonroad.scenario.scenario import Scenario
from commonroad.scenario.state import CustomState
from commonroad.scenario.trajectory import Trajectory
from commonroad.visualization.draw_params import DynamicObstacleParams
from commonroad.visualization.mp_renderer import MPRenderer
from commonroad_dc.geometry.util import resample_polyline
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from commonroad_reach.pycrreach import ReachPolygon
from matplotlib import pyplot as plt

from micp_planner.common.configuration import Configuration, VehicleConfiguration
from micp_planner.common.utils import getOrientationAtStation
from micp_planner.common.vehicle_model import VehicleModel


def showScenarioAndPlanningProblem(scenario: Scenario, planning_problem: PlanningProblem,
                                   ccosy: CurvilinearCoordinateSystem, time_step: int):
    fig, axs = plt.subplots(figsize=(35, 20))

    # Scenario and Planning Problem
    rnd = MPRenderer(ax=axs)
    rnd.draw_params.time_begin = time_step
    scenario.draw(rnd)
    planning_problem.draw(rnd)
    rnd.render(show=False)

    # Reference Path
    rp = np.asarray(ccosy.reference_path())
    axs.plot(rp[:, 0], rp[:, 1], zorder=16, marker='', color='red', linewidth=3)

    # Projection Domain
    proj_domain_border = np.asarray(ccosy.projection_domain())
    axs.plot(proj_domain_border[:, 0], proj_domain_border[:, 1], zorder=100, color='orange')

    plt.show()


def getCREgoObject(x: npt.NDArray, u: npt.NDArray, scenario, ccosy, vehicle_params: VehicleConfiguration,
                   time_step_increment: int) -> DynamicObstacle:
    # Generate state list based on trajectory
    state_list = []
    for i in range(x.shape[1]):
        # add new state to state_list
        position = ccosy.convert_to_cartesian_coords(x[0, i], 0.0)
        orientation = getOrientationAtStation(ccosy, x[0, i])

        steering_angle = 0
        # steering_angle = math.atan2((getOrientationAtStation(ccosy, x[0,i+1]) - orientation) * vehicle_params.wheelbase, x[1,i])

        new_state = CustomState(position=np.array([position[0], position[1]]),
                                orientation=orientation,
                                velocity=x[1, i],
                                # acceleration=u[0, i],
                                steering_angle=steering_angle,
                                time_step=i * time_step_increment)

        # TODO This loop is currently required due to a bug in CR io for plotting of obstacles states.
        # Probably in trajectory.py -> state_at_time_step() -> len(..)
        for j in range(time_step_increment):
            state_list.append(new_state)

    cr_trajectory = Trajectory(initial_time_step=state_list[0].time_step, state_list=state_list)

    # create the ego vehicle prediction using the trajectory and the shape of the obstacle
    dynamic_obstacle_initial_state = cr_trajectory.state_list[0]
    dynamic_obstacle_shape = Rectangle(length=vehicle_params.length, width=vehicle_params.width)
    dynamic_obstacle_prediction = TrajectoryPrediction(cr_trajectory, dynamic_obstacle_shape)

    # generate the dynamic obstacle according to the specification
    dynamic_obstacle_id = scenario.generate_object_id()
    dynamic_obstacle_type = ObstacleType.CAR
    dynamic_obstacle = DynamicObstacle(dynamic_obstacle_id,
                                       dynamic_obstacle_type,
                                       dynamic_obstacle_shape,
                                       dynamic_obstacle_initial_state,
                                       dynamic_obstacle_prediction)

    return dynamic_obstacle


def plot_solution(planner_name: str, x_opt: List, u_opt: List, obj_opt: List, rulebook, scenario: Scenario,
                  planning_problem: PlanningProblem, ccosy: CurvilinearCoordinateSystem,
                  vehicle_model: VehicleModel, obstacles: Dict,
                  reachable_set: List[List[ReachPolygon]], kinematic_speed_limit_segments: List,
                  config: Configuration):

    plot_dir = os.path.join(config.general.path_output, str(scenario.scenario_id), planner_name)
    os.makedirs(plot_dir, exist_ok=True)

    if config.debug.plot_v_t:
        visualize_solution(x_opt, u_opt, obj_opt, vehicle_model, plot_dir, config)
        visualize_solution_3D(x_opt, u_opt, obj_opt, vehicle_model, kinematic_speed_limit_segments, plot_dir, config)

    if config.debug.plot_scenario:
        visualizeScenario(x_opt, u_opt, scenario, planning_problem, config, ccosy, obstacles, reachable_set, plot_dir,
                          plot_ego_icons=True, plot_only_final_ego=False)

    if config.debug.plot_reachable_set:
        visualizeReachableSet(reachable_set, plot_dir, config, overlap=True, create_video=True, X=x_opt)

    if config.debug.plot_predicate_robustness:
        # Plot robustness of predicates for final time trajectory
        predicate_robustness = rulebook.predicate_robustness(x_opt[-1], u_opt[-1])
        plotPredicateRobustness(predicate_robustness, plot_dir, config)


def visualize_solution(X: npt.NDArray, U: npt.NDArray, obj: List, vehicle_model: VehicleModel, plot_dir: str,
                       config: Configuration):
    cmap = mpl.cm.get_cmap(config.debug.color_map)
    norm = mpl.colors.Normalize(vmin=0.0, vmax=len(X))

    K = config.planning.planning_horizon

    fig, axs = plt.subplots(3, 1, figsize=(10, 13))

    # ----- s-t plot -----
    ax = axs[0]

    ax.fill_between(range(K + 1), vehicle_model.getLowerBound(X[0][:, 0], K)[0, :],
                    vehicle_model.getUpperBound(X[0][:, 0], K)[0, :],
                    color='orange', alpha=.1, edgecolor=None)

    # create labels
    nof_opt_problems = len(obj)
    labels = []
    rho_min = [[], *[[min(0.0, round(r, 3)) for r in obj[:i]] for i in range(1, nof_opt_problems)]]
    for i in range(nof_opt_problems - 1):
        rho_opt = round(obj[i], 3)
        labels.append(r'$\rho_{min}$=' + str(rho_min[i]) + r', $\rho*$=' + str(rho_opt))
    labels.append(r'$\rho_{min}$=' + str(rho_min[-1]) + r', $q*$=' + str(round(obj[-1], 3)))

    for i, x in enumerate(X):
        ax.plot(x[0, :], 'o-', linewidth=1.5, zorder=500, label=f'P{i}: ' + labels[i], color=cmap(norm(i)))

    ax.set_xlim([0, K])

    ax.grid(which='minor', alpha=0.2)
    ax.grid(which='major', alpha=0.5)

    ax.set_xlabel('time in s')
    ax.set_ylabel('s in m')

    ax.legend()

    # ----- v-t plot -----
    ax = axs[1]

    ax.fill_between(range(K + 1), vehicle_model.getLowerBound(X[0][:, 0], K)[1, :],
                    vehicle_model.getUpperBound(X[0][:, 0], K)[1, :], color='orange', alpha=.1, edgecolor=None)

    for i, x in enumerate(X):
        ax.plot(x[1, :], 'o-', linewidth=1.5, zorder=500, label=f'Problem: {i}', color=cmap(norm(i)))

    ax.set_xlim([0, K])

    ax.grid()

    ax.set_xlabel('time in s')
    ax.set_ylabel('v in m/s')

    ax.legend()

    # ----- a-t plot -----
    ax = axs[2]

    for i, u in enumerate(U):
        ax.step(list(range(len(u[0, :]))), u[0, :], where='post', label=f'Problem: {i}', color=cmap(norm(i)))

    ax.set_xlim([0, K])

    ax.grid()

    ax.set_xlabel('time in s')
    ax.set_ylabel('a in m/s^2')

    ax.legend()

    # -----
    fig.tight_layout()

    plt.savefig(f'{plot_dir}/solution.png', format='png', dpi=300)
    plt.close()


def visualize_solution_3D(X: npt.NDArray, U: npt.NDArray, obj: List, vehicle_model: VehicleModel,
                          kinematic_speed_limit_segments: List[List], plot_dir: str, config: Configuration):
    cmap = mpl.cm.get_cmap(config.debug.color_map)
    norm = mpl.colors.Normalize(vmin=0.0, vmax=len(X))

    K = config.planning.planning_horizon

    s_range = [min(min(x[0, :]) for x in X), max(max(x[0, :]) for x in X)]
    v_range = [min(min(x[1, :]) for x in X), max(max(x[1, :]) for x in X)]
    k_range = [0, K]

    fig = plt.figure(figsize=(13, 13))

    # create labels
    nof_opt_problems = len(obj)
    labels = []
    rho_min = [[], *[[min(0.0, round(r, 3)) for r in obj[:i]] for i in range(1, nof_opt_problems)]]
    for i in range(nof_opt_problems - 1):
        rho_opt = round(obj[i], 3)
        labels.append(r'$\rho_{min}$=' + str(rho_min[i]) + r', $\rho*$=' + str(rho_opt))
    labels.append(r'$\rho_{min}$=' + str(rho_min[-1]) + r', $q*$=' + str(round(obj[-1], 3)))

    # ----- 3d plot -----
    ax = fig.add_subplot(221, projection='3d')

    ax.set_xlabel('position [m]')
    ax.set_ylabel('velocity [m/s]')
    ax.set_zlabel('time steps')
    time_steps = [i for i in range(len(X[0][0, :]))]
    for i, x in enumerate(X):
        ax.plot3D(x[0, :], x[1, :], time_steps, '-', label=f'P{i}', color=cmap(norm(i)))
    ax.view_init(-170, 56)
    ax.set_xlim(s_range)
    ax.set_ylim(v_range)
    ax.set_zlim(k_range)

    # ----- v-s plot -----
    ax = fig.add_subplot(223)
    for i, x in enumerate(X):
        ax.plot(x[0, :], x[1, :], 'o-', linewidth=1.5, zorder=500, label=f'P{i}: ' + labels[i], color=cmap(norm(i)))
    for i, kinematic_speed_limit_segment in enumerate(kinematic_speed_limit_segments):
        ax.fill_between([kinematic_speed_limit_segment[0], kinematic_speed_limit_segment[1]], [0.0, 0.0],
                        [kinematic_speed_limit_segment[2], kinematic_speed_limit_segment[2]], color='orange', alpha=.1,
                        edgecolor=None)

    ax.grid(which='major', alpha=0.5)

    ax.set_xlabel('position [m]')
    ax.set_ylabel('velocity [m/s]')
    ax.set_xlim(s_range)
    ax.set_ylim(v_range)

    ax.legend(prop={'size': 6})

    # ----- v-t plot -----
    ax = fig.add_subplot(222)
    ax.fill_between(range(K + 1), vehicle_model.getLowerBound(X[0][:, 0], K)[1, :],
                    vehicle_model.getUpperBound(X[0][:, 0], K)[1, :], color='orange', alpha=.1, edgecolor=None)
    for i, x in enumerate(X):
        ax.plot(x[1, :], 'o-', linewidth=1.5, zorder=500, label=f'P{i}', color=cmap(norm(i)))
    ax.set_xlim([0, K])
    # ax.set_ylim([0, self._vehicle_model.max_velocity])

    ax.grid(which='major', alpha=0.5)

    ax.set_xlabel('time steps')
    ax.set_ylabel('velocity [m/s]')
    ax.set_xlim(k_range)
    ax.set_ylim(v_range)

    ax.legend()

    # ----- s-t plot -----
    ax = fig.add_subplot(224)
    ax.fill_between(range(K + 1), vehicle_model.getLowerBound(X[0][:, 0], K)[0, :],
                    vehicle_model.getUpperBound(X[0][:, 0], K)[0, :],
                    color='orange', alpha=.1, edgecolor=None)
    for i, x in enumerate(X):
        ax.plot(x[0, :], 'o-', linewidth=1.5, zorder=500, label=f'P{i}', color=cmap(norm(i)))

    ax.set_xlim(k_range)
    ax.set_ylim(s_range)

    ax.grid(which='major', alpha=0.5)

    ax.set_xlabel('time steps')
    ax.set_ylabel('position [m]')

    ax.legend()

    # -----
    plt.savefig(f'{plot_dir}/solution_3d.png', format='png', dpi=300)
    plt.close()


def visualizeScenario(X: npt.NDArray, U: npt.NDArray, scenario: Scenario, planning_problem: PlanningProblem,
                      config: Configuration, ccosy: CurvilinearCoordinateSystem, obstacles: Dict, reachable_sets: List,
                      plot_dir: str, plot_ego_icons=True, plot_only_final_ego=False):
    cmap = mpl.cm.get_cmap(config.debug.color_map)
    norm = mpl.colors.Normalize(vmin=0.0, vmax=len(X))

    time_step_increment = int(config.planning.dt / scenario.dt)

    # Create CommonRoad Ego Objects
    ego_objects = []
    for i, x in enumerate(X):
        ego_objects.append(getCREgoObject(x, U[i], scenario, ccosy, config.vehicle, time_step_increment))

    # Make folder for images
    output_dir_images = os.path.join(plot_dir, 'scenario_time_steps')
    os.makedirs(output_dir_images, exist_ok=True)

    fig, axs = plt.subplots(figsize=(35, 20))

    for i in range(config.planning.planning_horizon + 1):
        axs.clear()
        if config.debug.ego_centered_view:
            ego_centered_view_size = config.debug.ego_centered_view_size
            rnd = MPRenderer(ax=axs,
                             plot_limits=[
                                 ego_objects[-1].prediction.trajectory.state_list[i * time_step_increment].position[
                                     0] - ego_centered_view_size,
                                 ego_objects[-1].prediction.trajectory.state_list[i * time_step_increment].position[
                                     0] + ego_centered_view_size,
                                 ego_objects[-1].prediction.trajectory.state_list[i * time_step_increment].position[
                                     1] - ego_centered_view_size,
                                 ego_objects[-1].prediction.trajectory.state_list[i * time_step_increment].position[
                                     1] + ego_centered_view_size])
        else:
            rnd = MPRenderer()

        rnd.draw_params.time_begin = i * time_step_increment
        rnd.draw_params.dynamic_obstacle.draw_icon = True
        rnd.draw_params.dynamic_obstacle.show_label = False
        rnd.draw_params.planning_problem.initial_state.state.draw_arrow = False

        scenario.draw(rnd)
        planning_problem.draw(rnd)

        obs_used_params = DynamicObstacleParams()
        obs_used_params.trajectory.zorder = 100
        obs_used_params.time_begin = i * time_step_increment
        obs_used_params.draw_icon = True
        obs_used_params.vehicle_shape.occupancy.shape.facecolor = 'orange'

        obs_ignored_params = deepcopy(obs_used_params)
        obs_ignored_params.vehicle_shape.occupancy.shape.facecolor = 'tomato'

        # Recolor the used and ignored obstacles
        for obstacle_id in obstacles.get('used'):
            scenario.obstacle_by_id(obstacle_id).draw(rnd, draw_params=obs_used_params)
        for obstacle_id in obstacles.get('ignored'):
            scenario.obstacle_by_id(obstacle_id).draw(rnd, draw_params=obs_ignored_params)

        # Ego vehicles
        ego_params = deepcopy(obs_used_params)
        if plot_ego_icons:
            for j, ego_object in enumerate(ego_objects):
                ego_params.vehicle_shape.occupancy.shape.facecolor = cmap(norm(j))
                ego_params.vehicle_shape.occupancy.shape.zorder = 32 + j
                if not plot_only_final_ego or j == len(ego_objects) - 1:
                    ego_object.draw(rnd, draw_params=ego_params)

        rnd.render(show=False)

        if not plot_ego_icons:
            for j, ego_object in enumerate(ego_objects):
                ego_pos = ego_object.prediction.trajectory.state_list[i].position
                if not plot_only_final_ego or j == len(ego_objects) - 1:
                    axs.plot(ego_pos[0], ego_pos[1], 'o', markersize=20, zorder=25, color=cmap(norm(j)))

        # Plot reachable set
        for reach_set in reachable_sets[i]:
            p_x, p_y = np.transpose(getPolylineFromReachableSet(ccosy, reach_set))
            axs.plot(p_x, p_y, zorder=25, marker='o', color='deepskyblue', linewidth=8, alpha=0.5)

        # Draw ref path
        rp = np.asarray(ccosy.reference_path())
        axs.plot(rp[:, 0], rp[:, 1], zorder=16, marker='', color='red', linewidth=3)

        # draw projection domain
        if config.debug.draw_projection_domain:
            proj_domain_border = np.asarray(ccosy.projection_domain())
            axs.plot(proj_domain_border[:, 0], proj_domain_border[:, 1], zorder=100, color='orange')

        fig.tight_layout()
        fig.savefig(output_dir_images + '/time_step_' + "{:04d}".format(i) + '.png')
        plt.close()

    # Create Video
    if config.debug.create_video:
        frame_rate = int(1.0 / (scenario.dt * time_step_increment))
        os.system("ffmpeg -y -r " + str(
            frame_rate) + " -f image2 -i " + output_dir_images + "/time_step_%04d.png " + plot_dir + "/time_steps.mp4")


def plotReachablePositionIntervals(K, polygons, vehicle_model, x_0):
    plt.figure()
    plt.fill_between(range(K + 1), vehicle_model.getLowerBound(x_0, K)[0, :], vehicle_model.getUpperBound(x_0, K)[0, :],
                     color='tab:orange', alpha=.1, edgecolor=None)
    for i in range(K + 1):
        for poly in polygons[i]:
            plt.plot((i, i), (poly.p_min, poly.p_max), 'tab:orange', linewidth=3)
    plt.grid(which='minor', alpha=0.2)
    plt.grid(which='major', alpha=0.5)
    plt.xlabel('time steps')
    plt.ylabel('s in m')
    plt.show()


def visualizeReachableSet(polygons, output_dir: str, config: Configuration, overlap: bool = False,
                          create_video: bool = False, dt_video: float = 0.25, X=[]):
    # Make folder for images
    output_dir_images = os.path.abspath(output_dir + "/reach_sets")
    os.makedirs(output_dir_images, exist_ok=True)

    fig, axs = plt.subplots()

    p_values = [vertex[0] for time_step in polygons for polygon in time_step for vertex in polygon.vertices]
    v_values = [vertex[1] for time_step in polygons for polygon in time_step for vertex in polygon.vertices]
    axs.set_xlim([min(p_values) - 1.0, max(p_values) + 1.0])
    axs.set_ylim([min(v_values) - 1.0, max(v_values) + 1.0])

    axs.grid(which='minor', alpha=0.2)
    axs.grid(which='major', alpha=0.5)

    axs.set_axisbelow(True)

    plt.xlabel('position [m]')
    plt.ylabel('velocity [m/s]')

    colors = np.linspace(0, 1, len(polygons))
    cmap = plt.cm.hsv

    cmap_states = mpl.cm.get_cmap(config.debug.color_map)
    norm = mpl.colors.Normalize(vmin=0.0, vmax=len(X))

    # ----- v-s plot -----

    for i, time_step in enumerate(polygons):

        plt.title(f'Time step: {i}')

        # Plot polygons
        for polygon in time_step:
            axs.add_patch(mpl.patches.Polygon(np.array(polygon.vertices), closed=True, facecolor='lightgray', alpha=0.7,
                                              edgecolor='gray'))

        # Plot states
        for j, x in enumerate(X):
            if overlap:
                axs.plot(x[0, :i + 1], x[1, :i + 1], 'o-', markersize=2, linewidth=0.5, label=f'P{j}' if i == 0 else "",
                         color=cmap_states(norm(j)))
            else:
                axs.plot(x[0, i], x[1, i], 'o' if overlap else 'o', markersize=2, linewidth=0.5, label=f'P{j}',
                         color=cmap_states(norm(j)))
        # Add Labels
        plt.legend()

        fig.savefig(output_dir_images + '/time_step_' + "{:04d}".format(i) + '.png', dpi=300)

        if not overlap:
            axs.patches.clear()
            [axs.lines.pop() for x in X]

    # Create Video
    if create_video:
        frame_rate = int(1.0 / dt_video)
        os.system("ffmpeg -y -r " + str(
            frame_rate) + " -f image2 -i " + output_dir_images + "/time_step_%04d.png " + output_dir_images + "/time_steps.mp4")


def plotScenarioWithReachablePosIntervals(reachable_sets, planning_problem, scenario, x_0, ccosy, dt, output_dir: str,
                                          create_video=False, draw_ref_path=True):
    # Make folder for images
    output_dir_images = os.path.abspath(os.getcwd() + output_dir + "/time_steps")
    os.makedirs(output_dir_images, exist_ok=True)

    fig, axs = plt.subplots(figsize=(35, 20))

    # ----- s-t plot -----

    fig.tight_layout()
    for i in range(len(reachable_sets)):
        axs.clear()

        ego_centered_view_size = 30
        initial_state = ccosy.convert_to_cartesian_coords(x_0[0], 0.0)
        rnd = MPRenderer(ax=axs, plot_limits=[initial_state[0] - ego_centered_view_size,
                                              initial_state[0] + ego_centered_view_size,
                                              initial_state[1] - ego_centered_view_size,
                                              initial_state[1] + ego_centered_view_size])

        rnd.draw_params.time_begin = i
        rnd.draw_params.dynamic_obstacle.draw_icon = True
        rnd.draw_params.dynamic_obstacle.show_label = True
        rnd.draw_params.planning_problem.initial_state.state.draw_arrow = False

        scenario.draw(rnd)
        planning_problem.draw(rnd)
        rnd.render(show=False)

        # Draw ref path
        if draw_ref_path:
            rp = np.asarray(ccosy.reference_path())
            axs.plot(rp[:, 0], rp[:, 1], zorder=16, marker='', color='red', linewidth=3)

        for reach_set in reachable_sets[i]:
            p_x, p_y = np.transpose(getPolylineFromReachableSet(ccosy, reach_set))
            axs.plot(p_x, p_y, zorder=50, marker='', color='g', linewidth=5)

        fig.savefig(output_dir_images + '/time_step_' + "{:04d}".format(i) + '.png')

    # Create Video
    if create_video:
        frame_rate = int(1.0 / dt)
        os.system("ffmpeg -y -r " + str(
            frame_rate) + " -f image2 -i " + output_dir_images + "/time_step_%04d.png " + output_dir_images + "/time_steps.mp4")


def getPolylineFromReachableSet(ccosy: CurvilinearCoordinateSystem, reachable_set: ReachPolygon) -> List:
    polyline_ccosy = [[reachable_set.p_min, 0.0], [reachable_set.p_max, 0.0]]
    polyline_ccosy = resample_polyline(polyline_ccosy, 0.5)
    polyline_cart = []
    for point in polyline_ccosy:
        if point[0] < 0.1:
            point[0] = 0.1
        if point[0] > ccosy.length() - 0.1:
            point[0] = ccosy.length() - 0.1
        polyline_cart.append(ccosy.convert_to_cartesian_coords(point[0], point[1]))
    return polyline_cart


def plotPredicateRobustness(predicate_robustness: List[Dict], plot_dir: str, config: Configuration):
    # Make output folder
    output_dir_images = os.path.abspath(plot_dir + "/predicate_robustness")
    os.makedirs(output_dir_images, exist_ok=True)

    for spec in predicate_robustness:
        # Define the size of each subplot
        subplot_size = (8, 3)
        nof_subplots = len(spec)

        # Create the figure and axes
        fig, axs = plt.subplots(nrows=nof_subplots, ncols=1, figsize=(subplot_size[0], subplot_size[1] * nof_subplots))

        # Loop over the axes and plot on each subplot
        for i, ax in enumerate(axs):
            rob = spec[i]

            for time_step in rob['evaluated_time_steps']:
                ax.axvspan(time_step - 0.4, time_step + 0.4, alpha=0.15, facecolor='lightblue')

            if rob['k_decisive'] is not None:
                k_decisive = rob['k_decisive']
                ax.axvspan(k_decisive - 0.4, k_decisive + 0.4, alpha=0.3, facecolor='orange')

            ax.axhline(y=0, color='black', linewidth=1.5)
            ax.plot(rob['robustness'], marker='.', color='r', label=rob['name'])

            ax.set_xlabel('time steps')
            ax.set_ylabel('robustness')

            ax.set_xlim([0, config.planning.planning_horizon])
            rho_max = max(abs(min(rob['robustness'])), abs(max(rob['robustness']))) + 0.5
            ax.set_ylim([-rho_max, rho_max])

            ax.legend(fontsize=8)

            ax.grid(True)

        fig.tight_layout()

        fig.savefig(output_dir_images + '/' + rob['spec_name'] + '.pdf')
