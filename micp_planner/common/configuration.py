from typing import Union

# commonroad-io
from commonroad.common.solution import VehicleType
# commonroad-dc
from commonroad_dc.feasibility.vehicle_dynamics import VehicleParameterMapping
from omegaconf import ListConfig, DictConfig


class Configuration:
    """
    Main Configuration class holding all planner-relevant configurations
    """
    def __init__(self, config: Union[ListConfig, DictConfig]):
        # initialize subclasses
        self.planning: PlanningConfiguration = PlanningConfiguration(config.planning)
        self.vehicle: VehicleConfiguration = VehicleConfiguration(config.vehicle)
        self.debug: DebugConfiguration = DebugConfiguration(config.debug)
        self.general: GeneralConfiguration = GeneralConfiguration(config.general)
        self.rulebook: RulebookConfiguration = RulebookConfiguration(config.rulebook)


class PlanningConfiguration:
    """Class to store all planning configurations"""
    def __init__(self, config: Union[ListConfig, DictConfig]):
        self.dt = config.dt
        self.planning_horizon = config.planning_horizon
        self.nof_considered_obstacles = config.nof_considered_obstacles
        self.extrapolate_obstacle_prediction = config.extrapolate_obstacle_prediction
        self.nof_reachable_pos_intervals = config.nof_reachable_pos_intervals
        self.nof_max_speeds = config.nof_max_speeds
        self.nof_min_speeds = config.nof_min_speeds
        self.nof_kinematic_speed_limit_segments = config.nof_kinematic_speed_limit_segments
        self.reach_init_uncertainty = config.reach_init_uncertainty


class VehicleConfiguration:
    """Class to store vehicle configurations"""
    def __init__(self, config: Union[ListConfig, DictConfig]):
        self.cr_vehicle_id = config.cr_vehicle_id

        # get vehicle parameters from CommonRoad vehicle models given cr_vehicle_id
        vehicle_parameters = VehicleParameterMapping.from_vehicle_type(VehicleType(config.cr_vehicle_id))

        # get dimensions from given vehicle ID
        self.length = vehicle_parameters.l
        self.width = vehicle_parameters.w
        self.wheelbase = vehicle_parameters.a + vehicle_parameters.b

        # get constraints from given vehicle ID
        self.a_min = -vehicle_parameters.longitudinal.a_max
        self.a_max = vehicle_parameters.longitudinal.a_max

        self.v_min = vehicle_parameters.longitudinal.v_min
        self.v_max = vehicle_parameters.longitudinal.v_max

        self.a_y_max = config.a_y_max

        # overwrite parameters given by vehicle ID if they are explicitly provided in the *.yaml file
        for key, value in config.items():
            if value is not None:
                setattr(self, key, value)


class DebugConfiguration:
    """Class to store debug configurations"""
    def __init__(self, config: Union[ListConfig, DictConfig]):
        self.plot_v_t = config.plot_v_t
        self.plot_scenario = config.plot_scenario
        self.plot_reachable_set = config.plot_reachable_set
        self.plot_predicate_robustness = config.plot_predicate_robustness
        self.ego_centered_view = config.ego_centered_view
        self.ego_centered_view_size = config.ego_centered_view_size
        self.draw_projection_domain = config.draw_projection_domain
        self.create_video = config.create_video

        self.color_map = config.color_map

        self.solver_verbose = config.solver_verbose
        self.calculate_iis = config.calculate_iis
        self.path_iis = config.path_iis


class GeneralConfiguration:
    def __init__(self, config: Union[ListConfig, DictConfig]):
        name_scenario = config.name_scenario

        self.path_scenarios = config.path_scenarios
        self.path_scenario = config.path_scenarios + name_scenario + ".xml"
        self.path_output = config.path_output


class RulebookConfiguration:
    def __init__(self, config: Union[ListConfig, DictConfig]):
        self.hierarchy = config.hierarchy
        self.hierarchy_single = config.hierarchy_single

        self.flatten_specifications = config.flatten_specifications

        self.epsilon = config.epsilon

        self.d_corr = config.d_corr
        self.k_c = config.k_c
        self.k_p = config.k_p

        self.a_min_obs = config.a_min_obs
        self.t_react = config.t_react

        self.fov_speed_limit = config.fov_speed_limit
        self.braking_speed_limit = config.braking_speed_limit
        self.road_condition_speed_limit = config.road_condition_speed_limit
        self.desired_lane_velocity = config.desired_lane_velocity

        self.u_abr = config.u_abr

        self.default_goal_position_interval = config.default_goal_position_interval

        self.delta_fl = config.delta_fl

        self.w_Q_00 = config.w_Q_00
        self.w_Q_11 = config.w_Q_11
        self.w_R_00 = config.w_R_00

        self.w_rho_ssc = config.w_rho_ssc
        self.w_rho_msc = config.w_rho_msc

        self.nu_pos_greater_than = config.nu_pos_greater_than
        self.nu_pos_lower_than = config.nu_pos_greater_than
        self.nu_vel_greater_than = config.nu_vel_greater_than
        self.nu_vel_lower_than = config.nu_vel_lower_than
        self.nu_acc_greater_than = config.nu_acc_greater_than
        self.nu_acc_lower_than = config.nu_acc_lower_than

        self.nu_brakes_abruptly_relative = config.nu_brakes_abruptly_relative
        self.nu_in_front_of = config.nu_in_front_of
        self.nu_in_same_lane = config.nu_in_same_lane
        self.nu_is_left = config.nu_is_left
        self.nu_orientation_is_positive = config.nu_orientation_is_positive
        self.nu_is_slow = config.nu_is_slow
        self.nu_keeps_safe_distance = config.nu_keeps_safe_distance
        self.nu_single_lane = config.nu_single_lane
