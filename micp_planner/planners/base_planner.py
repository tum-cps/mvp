import logging
import os
from abc import ABCMeta, abstractmethod
from typing import List

import gurobipy as gp
import numpy as np
import numpy.typing as npt
from tabulate import tabulate

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.vehicle_model import VehicleModel
from micp_planner.rulebook.rulebook import Rulebook


class PlanningSolution:
    def __init__(self, x_opt: List[npt.NDArray], u_opt: List[npt.NDArray], obj_opt: List[npt.NDArray],
                 model_time: float, solver_converged: bool, solver_statistics=None):
        self.x_opt = x_opt
        self.u_opt = u_opt
        self.obj_opt = obj_opt
        self.model_time = model_time
        self.solver_converged = solver_converged
        self.solver_statistics = solver_statistics


class BasePlanner(metaclass=ABCMeta):
    def __init__(self, rulebook: Rulebook, config: Configuration, name: str, verbose=False):
        """
        Constructor of the STL Planner
        """

        # Rulebook
        self.rulebook = rulebook
        self.nof_specifications = rulebook.nof_active_specifications()
        self.specifications = rulebook.get_active_specifications()

        # Parameters
        self.config = config
        self.K = config.planning.planning_horizon
        self.nof_reachable_pos_intervals = config.planning.nof_reachable_pos_intervals
        self.nof_kinematic_speed_limit_segments = config.planning.nof_kinematic_speed_limit_segments
        self.solver_verbose = config.debug.solver_verbose
        self.calculate_iis = config.debug.calculate_iis

        # Planner name
        self.name = name

        # Get vehicle model
        self.vehicle_model = VehicleModel(config.planning.dt, config.vehicle)

        # Get static bounds on control variables
        self.u_min = np.array([config.vehicle.a_min])
        self.u_max = np.array([config.vehicle.a_max])

        # Get static bounds on state variables (deactivated since reachability constraints are used)
        self.x_min = np.array([-gp.GRB.INFINITY, -gp.GRB.INFINITY])
        self.x_max = np.array([gp.GRB.INFINITY, gp.GRB.INFINITY])

        # Get parameters for quadratic cost function
        self.Q = np.diag([config.rulebook.w_Q_00, config.rulebook.w_Q_11])
        self.R = config.rulebook.w_R_00 * np.eye(1)

        # Solver instances
        self.solvers = []

        # Initialize instance
        self.createSolverInstances(verbose)

    @abstractmethod
    def createSolverInstances(self, verbose):
        pass

    def update_robustness_weights(self, w_rho):
        for solver in self.solvers:
            solver.setQuadraticNegativeRobustnessCostFunction(self.Q, self.R, w_rho)

    def update_and_plan(self, x_0: npt.NDArray, data: Data) -> PlanningSolution:
        self.rulebook.update(data)
        return self.plan(x_0, data)

    @abstractmethod
    def plan(self, x_0: npt.NDArray, data: Data) -> PlanningSolution:
        pass

    def handle_failed_optimization(self, solver):
        if self.calculate_iis:
            solver.model.computeIIS()
            solver.model.write(os.path.join(self.config.debug.path_iis, f"{self.name}_{solver.name}_iis.ilp"))
        logging.warning(f'Optimization failed for {self.name} {solver.name}.')

    def printSolversStatistics(self):
        headers = ['Name', 'Var.', 'Int. Var.', 'Constr.', 'z Constr.']
        solver_statistics = []
        for solver in self.solvers:
            solver_statistics.append([solver.name] + solver.getStatistics())

        print('\nSolver Statistics [Nof]:')
        print(tabulate(solver_statistics, headers=headers))
        print('\n')
