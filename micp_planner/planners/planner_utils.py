from typing import List

from micp_planner.common.configuration import Configuration
from micp_planner.common.utils import SuppressPrints
from micp_planner.planners.base_planner import BasePlanner
from micp_planner.planners.lexicographic_planner import LexicographicPlanner
from micp_planner.planners.multi_soft_constraint_planner import MultiSoftConstraintPlanner
from micp_planner.planners.single_hard_constraint_planner import SingleHardConstraintPlanner
from micp_planner.planners.single_soft_constraint_planner import SingleSoftConstraintPlanner
from micp_planner.rulebook.rulebook import Rulebook
from micp_planner.planners.run_planner import run_planner


def get_planner(planner: str, rulebook_multi: List[str], rulebook_single: List[str], config: Configuration) -> BasePlanner:

    # Single Hard Constraint Planner
    if planner == 'shc':
        config.rulebook.hierarchy = rulebook_single
        rb = Rulebook(config)
        return SingleHardConstraintPlanner(rb, config)

    # Single Soft Constraints Planner
    elif planner == 'ssc':
        config.rulebook.hierarchy = rulebook_single
        rb = Rulebook(config)
        return SingleSoftConstraintPlanner(rb, config)

    # Multiple Soft Constraints Planner
    elif planner == 'msc':
        config.rulebook.hierarchy = rulebook_multi
        rb = Rulebook(config)
        return MultiSoftConstraintPlanner(rb, config)

    # Lexicographic Planner
    elif planner == 'lex':
        config.rulebook.hierarchy = rulebook_multi
        rb = Rulebook(config)
        return LexicographicPlanner(rb, config)

    else:
        raise ValueError(f'Planner {planner} not implemented!')


def run_planner_with_context(planner, scenario, planning_problem, config, suppress_prints, make_plots=True):
    # If suppress_prints is True, suppress all print statements during the execution of the run_planner function
    if suppress_prints:
        with SuppressPrints():
            return run_planner(planner, scenario, planning_problem, config, make_plots=make_plots)
    # Otherwise, just execute the run_planner function as usual
    else:
        return run_planner(planner, scenario, planning_problem, config, make_plots=make_plots)
