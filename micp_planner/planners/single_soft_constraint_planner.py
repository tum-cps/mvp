import numpy.typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.planners.base_planner import BasePlanner, PlanningSolution
from micp_planner.rulebook.rulebook import Rulebook
from micp_planner.solvers.solver_gurobi import SolverGurobi


class SingleSoftConstraintPlanner(BasePlanner):
    """
    Single Soft-Constraint-Planner
    """

    def __init__(self, rulebook: Rulebook, config: Configuration, verbose=False):
        self.w_rho = config.rulebook.w_rho_ssc

        super().__init__(rulebook, config, 'ssc', verbose)

    def createSolverInstances(self, verbose: bool = False, save_solver_instances: bool = True):
        # Create Solver Instance
        solver = SolverGurobi("solver_0", self.vehicle_model, self.K, self.nof_reachable_pos_intervals,
                              self.nof_kinematic_speed_limit_segments, self.solver_verbose)
        self.solvers.append(solver)

        # Add constraints
        solver.addDecisionVariables()
        solver.setSystemDynamicsConstraints()
        solver.setControlBounds(self.u_min, self.u_max)
        solver.setStateBounds(self.x_min, self.x_max)
        solver.setReachabilityConstraints()
        solver.setKinematicSpeedLimitConstraints()

        # Add specification
        assert len(self.specifications) == 1, 'Only one specification is allowed for this planner'
        solver.addSpecification(self.specifications[0])

        # Add cost function
        assert self.w_rho is not None, 'w_rho must be provided for solver instance generation!'
        solver.setQuadraticNegativeRobustnessCostFunction(self.Q, self.R, self.w_rho)

        # print solver information
        if verbose:
            self.printSolversStatistics()

    def plan(self, x_0: npt.NDArray, data: Data) -> PlanningSolution:

        solver = self.solvers[0]  # We only have one solver
        solver.updateRankIndependentProperties(x_0, data.reachable_sets, data.kinematic_speed_limit_segments)

        # Solver
        x_opt, u_opt, obj_opt, runtime = solver.solve()

        if x_opt is None:
            self.handle_failed_optimization(solver)

        return PlanningSolution([x_opt], [u_opt], [obj_opt], runtime, x_opt is not None)

