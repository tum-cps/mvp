import numpy.typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.planners.base_planner import BasePlanner, PlanningSolution
from micp_planner.rulebook.rulebook import Rulebook
from micp_planner.solvers.solver_gurobi import SolverGurobi


class LexicographicPlanner(BasePlanner):
    """
    Lexicographic Planner
    """

    def __init__(self, rulebook: Rulebook, config: Configuration, verbose=False):
        super().__init__(rulebook, config, 'lex', verbose)

    def createSolverInstances(self, verbose: bool = False, save_solver_instances: bool = True):
        for i in range(self.nof_specifications + 1):
            solver = SolverGurobi("solver_" + str(i), self.vehicle_model, self.K, self.nof_reachable_pos_intervals,
                                  self.nof_kinematic_speed_limit_segments, self.solver_verbose)

            solver.addDecisionVariables()
            solver.setSystemDynamicsConstraints()
            solver.setControlBounds(self.u_min, self.u_max)
            solver.setStateBounds(self.x_min, self.x_max)
            solver.setReachabilityConstraints()
            solver.setKinematicSpeedLimitConstraints()

            # rank dependent solver setup
            for rank in range(self.nof_specifications):
                rho = solver.addSpecification(self.specifications[rank])
                if i == rank:
                    solver.setRobustnessCostFunction(rho)
                    self.solvers.append(solver)
                    break

            # final solver setup
            if i == self.nof_specifications:
                solver.setQuadraticCostFunction(self.Q, self.R)
                self.solvers.append(solver)

        # print solver information
        if verbose:
            self.printSolversStatistics()

    def plan(self, x_0: npt.NDArray, data: Data) -> PlanningSolution:
        x_opt, u_opt, obj_opt, rho_lbs = [], [], [], []

        # Solve lexicographic optimization problem
        model_runtime = 0.0
        for solver in self.solvers:

            solver.updateRankIndependentProperties(x_0, data.reachable_sets, data.kinematic_speed_limit_segments)
            solver.updateRobustnessConstraints(rho_lbs)

            x, u, obj_val, runtime = solver.solve()
            model_runtime += runtime

            if x is None:
                self.handle_failed_optimization(solver)

            x_opt.append(x)
            u_opt.append(u)
            obj_opt.append(obj_val)
            rho_lbs.append(min(0.0, obj_val))

        return PlanningSolution(x_opt, u_opt, obj_opt, model_runtime, not(any(element is None for element in x_opt)))
