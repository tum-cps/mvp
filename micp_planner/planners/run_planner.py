import time
from typing import Dict, List

from commonroad.planning.planning_problem import PlanningProblem
from commonroad.scenario.scenario import Scenario

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import InputDataHandler
from micp_planner.common.utils import createReferencePath, checkScenarioAndPlanningProblem
from micp_planner.common.visualization import plot_solution
from micp_planner.planners.base_planner import BasePlanner


def run_planner(planner: BasePlanner, scenario: Scenario, planning_problem: PlanningProblem, config: Configuration,
                verbose: bool = True, make_plots = True) -> Dict:
    # Print a log message about the planner and scenario
    print(f'Running {planner} planner for scenario {scenario.scenario_id}')

    # Check if the scenario and planning problem are valid with the given configuration
    checkScenarioAndPlanningProblem(scenario, planning_problem, config)

    # Create a reference path for the planning problem in the given scenario
    ccosy, route_lanelets = createReferencePath(planning_problem, scenario)
    # If a valid reference path could not be found, raise a warning
    if not ccosy:
        raise Warning(f'Route planner could not determine a valid route for scenario {scenario.scenario_id}!')

    # Initialize a data handler to handle input data for the planner
    data_handler = InputDataHandler(scenario, planning_problem, ccosy, route_lanelets, config)

    # Get the initial state for the planning problem
    x_0 = data_handler.mission.initial_state

    # Record the start time of the planning process
    comp_time_start = time.time()

    # Update input data and rulebook for the current state
    updated_input_data = data_handler.get(0, x_0)
    # If the reachable sets vanish over time, raise an error because a collision-free trajectory cannot be planned
    if updated_input_data.reachable_sets_vanish:
        raise ValueError('Reachable sets vanish over time. Collision free trajectory cannot be planned!')

    # Plan the trajectory using the planner
    sol = planner.update_and_plan(x_0, updated_input_data)

    # Calculate the planning time
    planning_time = time.time() - comp_time_start

    # If verbose is True, print additional details about the planning process
    if verbose:
        if sol.solver_converged:
            print("{:-^130s}".format(""))
            print("{:30s} {:8.5f} s".format("Planning time:", planning_time))
            print("{:30s} {:8.5f} s".format("Model time:", sol.model_time))
            print("{:30s} {}".format("rho*:", ', '.join(['{:8.5f}'.format(i) for i in sol.obj_opt[:-1]])))
            print("{:30s} {:8.5f}".format("q*:", sol.obj_opt[-1]))
            print("{:-^130s}".format(""))
        else:
            print("{:-^130s}".format(""))
            print('Solver did not converge!')
            print("{:-^130s}".format(""))

    # Plot the solution
    if make_plots and sol.solver_converged:
        plot_solution(planner.name, sol.x_opt, sol.u_opt, sol.obj_opt, planner.rulebook, scenario, planning_problem,
                      ccosy, planner.vehicle_model, updated_input_data.obstacles, updated_input_data.reachable_sets,
                      updated_input_data.kinematic_speed_limit_segments, config)

    # Prepare a dictionary of results
    results = {'scenario_id': str(scenario.scenario_id),
               'planning_time': planning_time,
               'model_time': sol.model_time,
               'obj_opt': sol.obj_opt}

    return results


def compare_planners(lex_planer: BasePlanner, other_planners: List[BasePlanner], scenario: Scenario,
                     planning_problem: PlanningProblem, config: Configuration) -> List:

    # Check if the scenario and planning problem are valid with the given configuration
    checkScenarioAndPlanningProblem(scenario, planning_problem, config)

    # Create a reference path for the planning problem in the given scenario
    ccosy, route_lanelets = createReferencePath(planning_problem, scenario)
    # If a valid reference path could not be found, raise a warning
    if not ccosy:
        raise Warning(f'Route planner could not determine a valid route for scenario {scenario.scenario_id}!')

    # Initialize a data handler to handle input data for the planner
    data_handler = InputDataHandler(scenario, planning_problem, ccosy, route_lanelets, config)

    # Get the initial state for the planning problem
    x_0 = data_handler.mission.initial_state

    # Update input data and rulebook for the current state
    updated_input_data = data_handler.get(0, x_0)
    # If the reachable sets vanish over time, raise an error because a collision-free trajectory cannot be planned
    if updated_input_data.reachable_sets_vanish:
        raise ValueError('Reachable sets vanish over time. Collision free trajectory cannot be planned!')

    # Loop over all planners
    solutions = []
    results = []
    for planner in [lex_planer] + other_planners:
        solutions.append(planner.update_and_plan(x_0, updated_input_data))

        lex_solution = solutions[0]
        current_solution = solutions[-1]

        m_score = None
        if current_solution.solver_converged:
            m_score = lex_planer.rulebook.m_score(current_solution.x_opt[-1], current_solution.u_opt[-1],
                                                  lex_solution.x_opt[-1], lex_solution.u_opt[-1])

        results.append({'planner': planner.name,
                        'scenario_id': str(scenario.scenario_id),
                        'converged': current_solution.solver_converged,
                        'model_time': current_solution.model_time,
                        'm_score': m_score})

    return results
