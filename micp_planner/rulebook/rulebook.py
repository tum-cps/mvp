from typing import List

import numpy as np
from numpy import typing as npt
from tabulate import tabulate

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.specifications.R_G1_G2_G3_G4_M1 import R_G1_G2_G3_G4_M1
from micp_planner.rulebook.specifications.R_G1_safe_distance import R_G1_SAFE_DISTANCE
from micp_planner.rulebook.specifications.R_G2_abrupt_braking import R_G2_ABRUPT_BRAKING
from micp_planner.rulebook.specifications.R_G3_maximum_speed_limit import R_G3_MAXIMUM_SPEED_LIMIT
from micp_planner.rulebook.specifications.R_G4_traffic_flow import R_G4_TRAFFIC_FLOW
from micp_planner.rulebook.specifications.R_M1_reach_goal import R_M1_REACH_GOAL


class Rulebook:
    """
    Rulebook
    """

    def __init__(self, config: Configuration, verbose: bool = False):

        self.config = config
        self.verbose = verbose

        self.hierarchy = self.config.rulebook.hierarchy

        # Instantiate all specifications
        specs = [R_G1_G2_G3_G4_M1(config),
                 R_G1_SAFE_DISTANCE(config),
                 R_G2_ABRUPT_BRAKING(config),
                 R_G3_MAXIMUM_SPEED_LIMIT(config),
                 R_G4_TRAFFIC_FLOW(config),
                 R_M1_REACH_GOAL(config)]

        self._available_specs = self.get_specs_mapping(specs)

        # Set total order of specifications
        self._active_specs = self.order_specs()

        if self.verbose:
            self.print_active_specifications()

    def get_active_specifications(self) -> List[Specification]:
        return self._active_specs

    def nof_active_specifications(self) -> int:
        return len(self._active_specs)

    def order_specs(self) -> List[Specification]:
        # Check if key exists in specifications
        for key in self.hierarchy:
            if key not in set(self._available_specs):
                raise KeyError('Attention: ' + key + ' does not specify an available specification!')

        # Check for duplicates
        if len(self.hierarchy) != len(set(self.hierarchy)):
            raise ValueError('Attention: Some keys are duplicated in the rulebook_old!')

        # Set active specifications
        active_specs = [self._available_specs[key].get() for key in self.hierarchy]

        # Flatten the specifications
        if self.config.rulebook.flatten_specifications:
            raise 'Formula flattening is not yet supported.'
            # for spec in active_specs:
            #     spec.stl_tree.simplify()

        return active_specs

    def update(self, data: Data):
        # Deactivate sub-formulas and predicates (e.g. when obstacles are not available)
        for spec in self._active_specs:
            # 1. Deactivate sub-specifications of specification
            spec.deactivate_sub_specifications(data)

            # 2. Update active predicates of specification
            for predicate in spec.predicates.values():
                predicate.update(data)

    def robustness(self, x: npt.NDArray, u: npt.NDArray, k: int = 0) -> List[float]:
        assert x.shape == (2, self.config.planning.planning_horizon + 1), 'Dimension of state signal is incorrect.'
        assert u.shape == (1, self.config.planning.planning_horizon + 1), 'Dimension of input signal is incorrect.'
        y = np.concatenate((x, u), axis=0)

        robustness = []
        for spec in self._active_specs:
            robustness.append(spec.robustness(y, k))
        return robustness

    def predicate_robustness(self, x: npt.NDArray, u: npt.NDArray) -> List:
        assert x.shape == (2, self.config.planning.planning_horizon + 1), 'Dimension of state signal is incorrect.'
        assert u.shape == (1, self.config.planning.planning_horizon + 1), 'Dimension of input signal is incorrect.'
        y = np.concatenate((x, u), axis=0)

        spec_pred_robustness = []
        for spec in self._active_specs:
            robustness, pred_name_decisive, k_decisive = spec.stl_tree.robustness_traced(y, k=0)

            pred_robs = []
            for pred_name, predicate in spec.predicates.items():
                if predicate.active:
                    rob = {'spec_name': spec.name,
                           'name': predicate.name,
                           'robustness': predicate.robustness_over_time_horizon(y)[0, :],
                           'evaluated_time_steps': predicate.evaluated_time_steps,
                           'k_decisive': k_decisive if predicate.name == pred_name_decisive else None}
                    pred_robs.append(rob)
            spec_pred_robustness.append(pred_robs)
        return spec_pred_robustness

    def get_predicate_by_name(self, name: str) -> STLTree:
        for spec in self._active_specs:
            predicate = spec.predicates.get(name)
            if predicate is not None:
                return predicate
        raise Warning(f'No predicate with name: {name} exists.')

    def get_deactivatable_sub_spec_by_name(self, name: str) -> STLTree:
        for spec in self._active_specs:
            sub_formula = spec.get_deactivatable_sub_spec(name)
            if sub_formula is not None:
                return sub_formula
        raise Warning(f'No deactivatable subformula with name: {name} exists.')

    def print_available_specifications(self):
        print('\n' + tabulate({'Rulebook': self._available_specs}, headers='keys', tablefmt="pretty") + '\n')

    def print_active_specifications(self):
        print('\n' + tabulate({'Rulebook': self.hierarchy}, headers='keys', tablefmt="pretty") + '\n')

    @staticmethod
    def get_specs_mapping(specs: List[Specification]) -> dict:
        return dict((spec.name, spec) for spec in specs)

    def m_score(self, x: npt.NDArray, u: npt.NDArray, x_lex_opt: npt.NDArray, u_lex_opt: npt.NDArray,
                k: int = 0) -> float:
        assert x.shape == (2, self.config.planning.planning_horizon + 1), 'Dimension of state signal is incorrect.'
        assert u.shape == (1, self.config.planning.planning_horizon + 1), 'Dimension of input signal is incorrect.'
        assert x_lex_opt.shape == (2, self.config.planning.planning_horizon + 1), 'Dimension of state signal is incorrect.'
        assert u_lex_opt.shape == (1, self.config.planning.planning_horizon + 1), 'Dimension of input signal is incorrect.'
        y = np.concatenate((x, u), axis=0)
        y_lex = np.concatenate((x_lex_opt, u_lex_opt), axis=0)

        m_score = 0

        for rank, spec in enumerate(self._active_specs):
            rho_y = spec.robustness(y, k)
            rho_y_lex = spec.robustness(y_lex, k)

            # when both robustness values are negative, evaluate how often y violates the specs more than y_lex
            if rho_y < rho_y_lex < 0.0:
                m_score += 1

        return m_score
