from micp_planner.common.configuration import Configuration
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.specifications.below_speed_limit import BELOW_SPEED_LIMIT


class R_G3_MAXIMUM_SPEED_LIMIT(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "R_G3_MAXIMUM_SPEED_LIMIT", config)

        self.sub_specs = {'sub_max_speed_limit': BELOW_SPEED_LIMIT(self.config, self.name)}

    def get_stl_tree(self) -> STLTree:
        k_final = self.config.planning.planning_horizon
        sub_velocity_is_lower_than = self.sub_specs['sub_max_speed_limit'].get_stl_tree()
        return sub_velocity_is_lower_than.always(0, k_final)
