from micp_planner.common.configuration import Configuration
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.predicates.pos_greater_than import POS_GREATER_THAN
from micp_planner.rulebook.predicates.pos_lower_than import POS_LOWER_THAN


class R_M1_REACH_GOAL(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "R_M1_REACH_GOAL", config)

    def get_stl_tree(self) -> STLTree:

        k_goal = self.config.planning.planning_horizon

        p_in_pos_interval = POS_GREATER_THAN(self.config, self.name + '_',
                                             b_func=lambda data: data.local_goal_position_interval.get('s_min')) \
                            & POS_LOWER_THAN(self.config, self.name + '_',
                                             b_func=lambda data: data.local_goal_position_interval.get('s_max'))

        return p_in_pos_interval.eventually(k_goal, k_goal)
