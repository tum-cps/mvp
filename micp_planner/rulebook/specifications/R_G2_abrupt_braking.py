from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.predicates.acc_lower_than import ACC_LOWER_THAN
from micp_planner.rulebook.predicates.brakes_abruptly_relative import BRAKES_ABRUPTLY_RELATIVE
from micp_planner.rulebook.predicates.in_front_of import IN_FRONT_OF
from micp_planner.rulebook.predicates.in_same_lane import IN_SAME_LANE
from micp_planner.rulebook.predicates.keeps_safe_distance_pwl import KEEPS_SAFE_DISTANCE_PWL


def sub_formula_name_factory_and(name: str, i: int) -> str:
    return name + f'_OBSTACLES_{i}_OR_'


def sub_formula_name_factory_or(name: str, i: int) -> str:
    return name + f'_OBSTACLES_{i}_AND_'


def prediction_fct_factory(i: int):
    def f(data: Data) -> Prediction:
        return data.predictions[i]
    return f


class R_G2_ABRUPT_BRAKING(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "R_G2_ABRUPT_BRAKING", config)

        self.nof_considered_obstacles = self.config.planning.nof_considered_obstacles

    def get_stl_tree(self) -> STLTree:

        a_abrupt = self.config.rulebook.u_abr
        k_goal = self.config.planning.planning_horizon

        # Create obstacle formulas OR connection part:
        obstacle_formulas_or = []
        for i in range(self.nof_considered_obstacles):
            sub_name = sub_formula_name_factory_or(self.name, i)

            p_in_same_lane = IN_SAME_LANE(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_in_front_of = IN_FRONT_OF(self.config, sub_name, b_func=prediction_fct_factory(i))

            sub_spec = p_in_same_lane & p_in_front_of

            obstacle_formulas_or.append(sub_spec)
            self.mark_for_deactivation(sub_spec, sub_name)

        # Concatenate the specification for each obstacle
        spec_obs_or = obstacle_formulas_or[0]
        for i in range(1, len(obstacle_formulas_or)):
            spec_obs_or = spec_obs_or | obstacle_formulas_or[i]

        # Create obstacle formulas AND connection part:
        obstacle_formulas_and = []
        for i in range(self.nof_considered_obstacles):
            sub_name = sub_formula_name_factory_and(self.name, i)

            p_not_keeps_safe_distance_prec = KEEPS_SAFE_DISTANCE_PWL(self.config, sub_name, a_b_func=prediction_fct_factory(i)).negation()

            p_not_brakes_abruptly_relative = BRAKES_ABRUPTLY_RELATIVE(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()
            p_not_in_same_lane = IN_SAME_LANE(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()
            p_not_in_front_of = IN_FRONT_OF(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()

            sub_spec = p_not_in_same_lane | p_not_in_front_of | p_not_keeps_safe_distance_prec | p_not_brakes_abruptly_relative

            obstacle_formulas_and.append(sub_spec)
            self.mark_for_deactivation(sub_spec, sub_name)

        # Concatenate the specification for each obstacle
        spec_obs_and = obstacle_formulas_and[0]
        for i in range(1, len(obstacle_formulas_and)):
            spec_obs_and = spec_obs_and & obstacle_formulas_and[i]

        p_not_brakes_abruptly = ACC_LOWER_THAN(self.config, self.name, b_static=a_abrupt).negation()
        p_not_is_breaking = ACC_LOWER_THAN(self.config, self.name, b_static=0.0).negation()

        spec = p_not_is_breaking | (p_not_brakes_abruptly | spec_obs_or) & spec_obs_and

        return spec.always(0, k_goal)

    def deactivate_sub_specs(self, data):
        # Deactivate sub-specifications corresponding to non-existing obstacles
        for i in range(len(data.predictions), self.nof_considered_obstacles):
            self.deactivate_sub_specification(sub_formula_name_factory_and(self.name, i))
            self.deactivate_sub_specification(sub_formula_name_factory_or(self.name, i))
