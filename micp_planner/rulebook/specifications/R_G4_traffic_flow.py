from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.predicates.in_front_of import IN_FRONT_OF
from micp_planner.rulebook.predicates.in_same_lane import IN_SAME_LANE
from micp_planner.rulebook.predicates.is_slow import IS_SLOW
from micp_planner.rulebook.specifications.preserves_flow import PRESERVES_FLOW


def sub_formula_name_factory(name: str, i: int) -> str:
    return name + f'_OBSTACLES_{i}_'


def prediction_fct_factory(i: int):
    def f(data: Data) -> Prediction:
        return data.predictions[i]
    return f


class R_G4_TRAFFIC_FLOW(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "R_G4_TRAFFIC_FLOW", config)

        self.sub_specs = {'sub_preserves_flow': PRESERVES_FLOW(self.config, self.name)}

        self.nof_considered_obstacles = self.config.planning.nof_considered_obstacles

    def get_stl_tree(self) -> STLTree:

        k_goal = self.config.planning.planning_horizon

        # Create obstacle formulas:
        obstacle_formulas = []
        for i in range(self.nof_considered_obstacles):
            sub_name = sub_formula_name_factory(self.name, i)

            p_in_same_lane = IN_SAME_LANE(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_in_front_of = IN_FRONT_OF(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_is_slow = IS_SLOW(self.config, sub_name, b_func=prediction_fct_factory(i))

            sub_tree = p_in_same_lane & p_in_front_of & p_is_slow

            obstacle_formulas.append(sub_tree)
            self.mark_for_deactivation(sub_tree, sub_name)

        # Concatenate the specification for each obstacle
        sub_tree = obstacle_formulas[0]
        for i in range(1, len(obstacle_formulas)):
            sub_tree = sub_tree | obstacle_formulas[i]

        sub_preserves_flow = self.sub_specs['sub_preserves_flow'].get_stl_tree()
        spec = sub_tree | sub_preserves_flow

        return spec.always(0, k_goal)

    def deactivate_sub_specs(self, data):
        # Deactivate sub-formulas corresponding to non-existing obstacles
        for i in range(len(data.predictions), self.nof_considered_obstacles):
            self.deactivate_sub_specification(sub_formula_name_factory(self.name, i))
