from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.predicates.pos_greater_than import POS_GREATER_THAN
from micp_planner.rulebook.predicates.pos_lower_than import POS_LOWER_THAN
from micp_planner.rulebook.predicates.vel_lower_than import VEL_LOWER_THAN


def sub_formula_name_factory(name: str, i: int) -> str:
    return name + f'_MAX_SPEEDS_{i}_'


def s_min_fct_factory(i: int):
    def f(data: Data) -> float:
        return data.max_speeds[i][0]
    return f


def s_max_fct_factory(i: int):
    def f(data: Data) -> float:
        return data.max_speeds[i][1]
    return f


def v_max_fct_factory(i: int):
    def f(data: Data) -> float:
        return data.max_speeds[i][2]
    return f


class BELOW_SPEED_LIMIT(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "_BELOW_SPEED_LIMIT", config)

        self.nof_max_speeds = self.config.planning.nof_max_speeds

    def get_stl_tree(self) -> STLTree:

        max_speed_formulas = []
        for i in range(self.nof_max_speeds):
            sub_name = sub_formula_name_factory(self.name, i)

            p_not_is_after_limit_start = POS_GREATER_THAN(self.config, sub_name, b_func=s_min_fct_factory(i)).negation()
            p_not_before_limit_end = POS_LOWER_THAN(self.config, sub_name, b_func=s_max_fct_factory(i)).negation()
            p_below_speed_limit = VEL_LOWER_THAN(self.config, sub_name, b_func=v_max_fct_factory(i))

            sub_spec = p_not_is_after_limit_start | p_not_before_limit_end | p_below_speed_limit

            max_speed_formulas.append(sub_spec)
            self.mark_for_deactivation(sub_spec, sub_name)

        # Concatenate the specifications
        spec = max_speed_formulas[0]
        for i in range(1, len(max_speed_formulas)):
            spec = spec & max_speed_formulas[i]

        return spec

    def deactivate_sub_specs(self, data):
        # Deactivate sub-specifications corresponding to non-existing maximum speeds
        for i in range(len(data.max_speeds), self.nof_max_speeds):
            self.deactivate_sub_specification(sub_formula_name_factory(self.name, i))
