from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.predicates.pos_greater_than import POS_GREATER_THAN
from micp_planner.rulebook.predicates.pos_lower_than import POS_LOWER_THAN
from micp_planner.rulebook.predicates.vel_greater_than import VEL_GREATER_THAN


def sub_formula_name_factory(name: str, i: int) -> str:
    return name + f'_MIN_SPEEDS_{i}_'


def s_min_fct_factory(i: int):
    def f(data: Data) -> float:
        return data.min_speeds[i][0]
    return f


def s_max_fct_factory(i: int):
    def f(data: Data) -> float:
        return data.min_speeds[i][1]
    return f


def v_min_fct_factory(i: int):
    def f(data: Data) -> float:
        return data.min_speeds[i][2]
    return f


class PRESERVES_FLOW(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "_PRESERVES_FLOW", config)

        self.nof_min_speeds = self.config.planning.nof_min_speeds

    def get_stl_tree(self) -> STLTree:

        min_speed_formulas = []
        for i in range(self.nof_min_speeds):
            sub_name = sub_formula_name_factory(self.name, i)

            p_not_is_after_limit_start = POS_GREATER_THAN(self.config, sub_name, b_func=s_min_fct_factory(i)).negation()
            p_not_before_limit_end = POS_LOWER_THAN(self.config, sub_name, b_func=s_max_fct_factory(i)).negation()
            p_above_required_speed = VEL_GREATER_THAN(self.config, sub_name, b_func=v_min_fct_factory(i))

            sub_spec = p_not_is_after_limit_start | p_not_before_limit_end | p_above_required_speed

            min_speed_formulas.append(sub_spec)
            self.mark_for_deactivation(sub_spec, sub_name)

        # Concatenate the specifications
        spec = min_speed_formulas[0]
        for i in range(1, len(min_speed_formulas)):
            spec = spec & min_speed_formulas[i]

        return spec

    def deactivate_sub_specs(self, data):
        # Deactivate sub-specifications corresponding to non-existing required speeds
        for i in range(len(data.min_speeds), self.nof_min_speeds):
            self.deactivate_sub_specification(sub_formula_name_factory(self.name, i))
