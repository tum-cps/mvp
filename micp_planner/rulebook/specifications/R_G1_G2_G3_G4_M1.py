from micp_planner.common.configuration import Configuration
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.specifications.R_G1_safe_distance import R_G1_SAFE_DISTANCE
from micp_planner.rulebook.specifications.R_G2_abrupt_braking import R_G2_ABRUPT_BRAKING
from micp_planner.rulebook.specifications.R_G3_maximum_speed_limit import R_G3_MAXIMUM_SPEED_LIMIT
from micp_planner.rulebook.specifications.R_G4_traffic_flow import R_G4_TRAFFIC_FLOW
from micp_planner.rulebook.specifications.R_M1_reach_goal import R_M1_REACH_GOAL


class R_G1_G2_G3_G4_M1(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "R_G1_G2_G3_G4_M1", config)

        self.sub_specs = {'sub_R_G1': R_G1_SAFE_DISTANCE(self.config, self.name),
                          'sub_R_G2': R_G2_ABRUPT_BRAKING(self.config, self.name),
                          'sub_R_G3': R_G3_MAXIMUM_SPEED_LIMIT(self.config, self.name),
                          'sub_R_G4': R_G4_TRAFFIC_FLOW(self.config, self.name),
                          'sub_R_M1': R_M1_REACH_GOAL(self.config, self.name)}

    def get_stl_tree(self) -> STLTree:

        R_G1 = self.sub_specs['sub_R_G1'].get_stl_tree()
        R_G2 = self.sub_specs['sub_R_G2'].get_stl_tree()
        R_G3 = self.sub_specs['sub_R_G3'].get_stl_tree()
        R_G4 = self.sub_specs['sub_R_G4'].get_stl_tree()
        R_M1 = self.sub_specs['sub_R_M1'].get_stl_tree()

        return R_G1 & R_G2 & R_G3 & R_G4 & R_M1
