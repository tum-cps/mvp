from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.specification import Specification
from micp_planner.rulebook.predicates.in_front_of import IN_FRONT_OF
from micp_planner.rulebook.predicates.in_same_lane import IN_SAME_LANE
from micp_planner.rulebook.predicates.is_left import IS_LEFT
from micp_planner.rulebook.predicates.orientation_is_positive import ORIENTATION_IS_POSITIVE
from micp_planner.rulebook.predicates.keeps_safe_distance_pwl import KEEPS_SAFE_DISTANCE_PWL
from micp_planner.rulebook.predicates.single_lane import SINGLE_LANE


def sub_formula_name_factory(name: str, i: int) -> str:
    return name + f'_OBSTACLES_{i}_'


def prediction_fct_factory(i: int):
    def f(data: Data) -> Prediction:
        return data.predictions[i]
    return f


class R_G1_SAFE_DISTANCE(Specification):
    def __init__(self, config: Configuration, name=''):
        super().__init__(name + "R_G1_SAFE_DISTANCE", config)

        self.nof_considered_obstacles = self.config.planning.nof_considered_obstacles

    def get_stl_tree(self) -> STLTree:

        k_c = self.config.rulebook.k_c
        k_p = self.config.rulebook.k_p
        k_goal = self.config.planning.planning_horizon

        # Create obstacle formulas:
        obstacle_formulas = []
        for i in range(self.nof_considered_obstacles):
            sub_name = sub_formula_name_factory(self.name, i)

            p_in_same_lane = IN_SAME_LANE(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_not_in_same_lane = IN_SAME_LANE(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()
            p_not_in_front_of = IN_FRONT_OF(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()
            p_single_lane = SINGLE_LANE(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_not_single_lane = SINGLE_LANE(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()
            p_is_left = IS_LEFT(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_not_is_left = IS_LEFT(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()
            p_orientation_is_positive = ORIENTATION_IS_POSITIVE(self.config, sub_name, b_func=prediction_fct_factory(i))
            p_not_orientation_is_positive = ORIENTATION_IS_POSITIVE(self.config, sub_name, b_func=prediction_fct_factory(i)).negation()

            p_keeps_safe_distance_prec = KEEPS_SAFE_DISTANCE_PWL(self.config, sub_name, a_b_func=prediction_fct_factory(i))

            p_cut_in = p_not_single_lane & p_in_same_lane &((p_is_left & p_not_orientation_is_positive) | (p_not_is_left & p_orientation_is_positive))
            p_not_cut_in = p_single_lane | p_not_in_same_lane | ((p_not_is_left | p_orientation_is_positive) & (p_is_left | p_not_orientation_is_positive))

            sub_spec = p_not_in_same_lane | p_not_in_front_of | (p_cut_in & p_not_cut_in.once(k_p, k_p)).once(0, k_c) | p_keeps_safe_distance_prec

            obstacle_formulas.append(sub_spec)
            self.mark_for_deactivation(sub_spec, sub_name)

        # Concatenate the specification for each obstacle
        spec = obstacle_formulas[0]
        for i in range(1, len(obstacle_formulas)):
            spec = spec & obstacle_formulas[i]
        return spec.always(0, k_goal)

    def deactivate_sub_specs(self, data):
        # Deactivate sub-formulas corresponding to non-existing obstacles
        for i in range(len(data.predictions), self.nof_considered_obstacles):
            self.deactivate_sub_specification(sub_formula_name_factory(self.name, i))
