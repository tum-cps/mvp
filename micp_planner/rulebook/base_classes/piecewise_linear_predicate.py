""" This file is based on predicate.py from STLpy package"""
from abc import abstractmethod
from typing import Dict, Callable, List, Tuple

import numpy as np
import numpy.typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.formula import STLFormula


class PiecewiseLinearPredicate(STLFormula):
    def __init__(self, config: Configuration,
                 a_static: List[npt.NDArray] = None,
                 b_static: List[npt.NDArray] = None,
                 a_func: Callable = None,
                 b_func: Callable = None,
                 subspaces: List[List[Tuple[npt.NDArray, npt.NDArray]]] = None,
                 nu: float = 1.0,
                 constraint_relation: ConstraintRelation = ConstraintRelation.GREATER_THAN,
                 name=None, y_dim: int = 3):

        # Check that one of the two is provided (a_static or a_func, b_static or b_func)
        assert (a_static is not None and a_func is None) or (a_func is not None and a_static is None), "Provide one!"
        assert (b_static is not None and b_func is None) or (b_func is not None and b_static is None), "Provide one!"

        self.K = config.planning.planning_horizon
        self.d = y_dim

        self.a_shape = (y_dim, self.K + 1)
        self.b_shape = (1, self.K + 1)

        self.subspaces = subspaces
        self.nof_subspaces = len(subspaces)

        self.a_sub = [np.zeros(self.a_shape) for i in range(self.nof_subspaces)]
        self.b_sub = [np.zeros(self.b_shape) for i in range(self.nof_subspaces)]

        self._a_func = a_func
        self._b_func = b_func

        self._alpha = constraint_relation.value / nu
        self._epsilon = 0.0

        self.name = name

        self.active = True
        self.is_parent_of_sub_spec = False

        self._config = config

        self._a_is_static = self.set_static_a(a_static)
        self._b_is_static = self.set_static_b(b_static)

        self.evaluated_time_steps = set()

    def deactivate(self):
        self.active = False

    def negation(self):
        # Check if predicate is to be negated multiple times
        assert 'not' not in self.name, "Negated predicate is to be negated again. This is prohibited!"

        if self.name is not None:
            self.name = self.name + "_NOT"

        self._alpha *= -1.0
        self._epsilon = self._config.rulebook.epsilon

        # In case parameters are static (beyond that, it has no other effect)
        self.a_sub = [-1.0 * a for a in self.a_sub]
        self.b_sub = [-1.0 * b + self._config.rulebook.epsilon for b in self.b_sub]

        return self

    def update(self, data: Dict):
        if self.active:
            for i in range(self.nof_subspaces):
                if not self._a_is_static:
                    self.update_a_sub(self.get_a_sub(data, i), i)
                if not self._b_is_static:
                    self.update_b_sub(self.get_b_sub(data, i), i)

    def update_a_sub(self, a: npt.NDArray, sub_id: int):
        self.a_sub[sub_id] = self._alpha * a
        assert self.a_sub[sub_id].shape == self.a_shape, 'a is defined incorrectly.'

    def update_b_sub(self, b: npt.NDArray, sub_id: int):
        self.b_sub[sub_id] = self._alpha * b + self._epsilon
        assert self.b_sub[sub_id].shape == self.b_shape, 'b is defined incorrectly.'

    def set_static_a(self, a_sub: List[npt.NDArray]) -> bool:
        if a_sub is not None:
            assert len(a_sub) == self.nof_subspaces
            for i, a in enumerate(a_sub):
                # Repeat a vector K+1 times
                a = np.array(a).reshape((-1, 1))
                self.update_a_sub(np.tile(a, (1, self.K + 1)), i)
            return True
        else:
            return False

    def set_static_b(self, b_sub: List[npt.NDArray]) -> bool:
        if b_sub is not None:
            for i, b in enumerate(b_sub):
                assert len(b_sub) == self.nof_subspaces
                # Repeat b vector K+1 times
                b = np.array(b).reshape((-1, 1))
                self.update_b_sub(np.tile(b, (1, self.K + 1)), i)
            return True
        else:
            return False

    @abstractmethod
    def get_a_sub(self, data: Dict, sub_id: int) -> npt.NDArray:
        pass

    @abstractmethod
    def get_b_sub(self, data: Dict, sub_id: int) -> npt.NDArray:
        pass

    def robustness(self, y, k):
        return min([(self.a_sub[i][:, k].T @ y[:, k] - self.b_sub[i][:, k]).item() for i in range(self.nof_subspaces)])


    def robustness_traced(self, y, k):
        self.evaluated_time_steps.add(k)
        i = self.find_subspace(y[:, k])
        return min([(self.a_sub[i][:, k].T @ y[:, k] - self.b_sub[i][:, k]).item() for i in range(self.nof_subspaces)]), self.name, k

    def robustness_over_time_horizon(self, y) -> npt.NDArray:
        rob = []
        for k in range(self.K + 1):
            rob.append(min([(self.a_sub[i][:, k].T @ y[:, k] - self.b_sub[i][:, k]).item() for i in range(self.nof_subspaces)]))
        return np.array(rob).reshape(1, -1)

    def is_predicate(self):
        return True

    def __str__(self):
        if self.name is None:
            return "{ Predicate %s*y + >= %s }" % (self.a, self.b)
        else:
            return "{ Predicate " + self.name + " }"

    def find_subspace(self, y_k) -> int:
        for i, subspace in enumerate(self.subspaces):
            is_inside = True
            for hyperplane in subspace:
                is_inside &= hyperplane[0].T @ y_k >= hyperplane[1].item()
            if is_inside:
                return i

        raise Warning("The subspaces were defined incorrectly, some states are note considered!")
