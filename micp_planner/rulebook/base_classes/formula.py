""" This file is based on Formula.py from STLpy package"""
from abc import ABC, abstractmethod

from treelib import Tree


class STLFormula(ABC):
    """
    An abstract class which encompasses represents all kinds of STL formulas :math:`\\varphi`, including
    predicates (the simplest possible formulas) and standard formulas (made up of logical operations over
    predicates and other formulas).
    """

    @abstractmethod
    def robustness(self, y, k):
        """
        Compute the robustness measure :math:`\\rho^\\varphi(y,k)` of this formula for the
        given signal :math:`y = y_0,y_1,\\dots,y_T`, evaluated at timestep :math:`k`.

        :param y:    A ``(d,K)`` numpy array representing the signal
                     to evaluate, where ``d`` is the dimension of
                     the signal and ``K`` is the number of timesteps
        :param k:    The timestep :math:`k` to evaluate the signal at. This
                     is typically 0 for the full formula.

        :return:    The robustness measure :math:`\\rho^\\varphi(y,k)` which is positive only
                    if the signal satisfies the specification.
        """
        pass

    @abstractmethod
    def robustness_traced(self, y, k):
        pass

    @abstractmethod
    def is_predicate(self):
        """
        Indicate whether this formula is a predicate.

        :return:    A boolean which is ``True`` only if this is a predicate.
        """
        pass

    @abstractmethod
    def deactivate(self):
        pass

    @abstractmethod
    def negation(self):
        """
        Return a new :class:`.STLFormula` :math:`\\varphi_{new}` which represents
        the negation of this formula:

        .. math::

            \\varphi_{new} = \lnot \\varphi

        :return: An :class:`.STLFormula` representing :math:`\\varphi_{new}`

        .. note::

            For now, only formulas in positive normal form are supported. That means that negation
            (:math:`\lnot`) can only be applied to predicates (:math:`\\pi`).

        """
        pass

    def conjunction(self, other):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which represents the conjunction
        (and) of this formula (:math:`\\varphi`) and another one (:math:`\\varphi_{other}`):

        .. math::

            \\varphi_{new} = \\varphi \land \\varphi_{other}

        :param other:   The :class:`.STLFormula` :math:`\\varphi_{other}`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`

        .. note::

            Conjuction can also be represented with the ``&`` operator, i.e.,
            ::

                c = a & b

            is equivalent to
            ::

                c = a.conjuction(b)

        """
        return STLTree([self,other],"and",[0,0])

    def __and__(self, other):
        """
        Syntatic sugar so we can write `one_and_two = one & two`
        """
        return self.conjunction(other)

    def disjunction(self, other):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which represents the disjunction
        (or) of this formula (:math:`\\varphi`) and another one (:math:`\\varphi_{other}`):

        .. math::

            \\varphi_{new} = \\varphi \lor \\varphi_{other}

        :param other:   The :class:`.STLFormula` :math:`\\varphi_{other}`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`

        .. note::

            Disjunction can also be represented with the ``|`` operator, i.e.,
            ::

                c = a | b

            is equivalent to
            ::

                c = a.disjunction(b)

        """
        return STLTree([self,other],"or",[0,0])

    def __or__(self, other):
        """
        Syntatic sugar so we can write `one_or_two = one | two`
        """
        return self.disjunction(other)

    def always(self, k1, k2):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which ensures that this
        formula (:math:`\\varphi`) holds for all of the timesteps between
        :math:`k_1` and :math:`k_2`:

        .. math::

            \\varphi_{new} = G_{[k_1,k_2]}(\\varphi)


        :param k1:  An integer representing the delay :math:`k_1`
        :param k2:  An integer representing the deadline :math:`k_2`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`
        """
        time_interval = [k for k in range(k1,k2+1)]
        subformula_list = [self for k in time_interval]
        formula = STLTree(subformula_list, "and", time_interval)
        if self.name is not None:
            formula.name = "always [%s,%s] %s" % (k1,k2,self.name)
        return formula

    def historically(self, k1, k2):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which ensures that this
        formula (:math:`\\varphi`) holds for all the timesteps between
        :math:`-k_2` and :math:`-k_1`:

        .. math::

            \\varphi_{new} = H_{[k_1,k_2]}(\\varphi)


        :param k1:  An integer representing the time :math:`k_1`
        :param k2:  An integer representing the time :math:`k_2`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`
        """
        time_interval = [k for k in range(-k2, -k1+1)]
        subformula_list = [self for k in time_interval]
        formula = STLTree(subformula_list, "and", time_interval)
        if self.name is not None:
            formula.name = "historically [%s,%s] %s" % (k1,k2,self.name)
        return formula

    def eventually(self, k1, k2):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which ensures that this
        formula (:math:`\\varphi`) holds for at least one timestep between
        :math:`k_1` and :math:`k_2`:

        .. math::

            \\varphi_{new} = F_{[k_1,k_2]}(\\varphi)


        :param k1:  An integer representing the delay :math:`k_1`
        :param k2:  An integer representing the deadline :math:`k_2`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`
        """
        time_interval = [k for k in range(k1,k2+1)]
        subformula_list = [self for k in time_interval]

        formula = STLTree(subformula_list, "or", time_interval)
        if self.name is not None:
            formula.name = "eventually [%s,%s] %s" % (k1,k2,self.name)
        return formula

    def once(self, k1, k2):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which ensures that this
        formula (:math:`\\varphi`) holds for at least one timestep between
        :math:`-k_2` and :math:`-k_1`:

        .. math::

            \\varphi_{new} = O_{[k_1,k_2]}(\\varphi)


        :param k1:  An integer representing the time :math:`k_1`
        :param k2:  An integer representing the time :math:`k_2`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`
        """
        time_interval = [k for k in range(-k2,-k1+1)]
        subformula_list = [self for k in time_interval]

        formula = STLTree(subformula_list, "or", time_interval)
        if self.name is not None:
            formula.name = "once [%s,%s] %s" % (k1,k2,self.name)
        return formula

    def until(self, other, k1, k2):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which ensures that the
        given formula :math:`\\varphi_{other}` holds for at least one timestep between
        :math:`k_1` and :math:`k_2`, and that this formula (:math:`\\varphi`) holds
        at all timesteps until then:

        .. math::

            \\varphi_{new} = \\varphi U_{[k_1,k_2]}(\\varphi_{other})

        :param other:   A :class:`.STLFormula` representing :math:`\\varphi_{other`
        :param k1:  An integer representing the delay :math:`k_1`
        :param k2:  An integer representing the deadline :math:`k_2`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`
        """
        # For every candidate switching time (k_prime), construct a subformula
        # representing 'self' holding until k_prime, at which point 'other' holds.
        self_until_kprime = []

        for k_prime in range(k1, k2+1):
            # NOTE: This describes now the classic definition of until and differs from the paper
            time_interval = [k for k in range(0, k_prime)] + [k_prime]
            subformula_list = [self for sf in range(0, k_prime)] + [other]

            self_until_kprime.append(STLTree(subformula_list, "and", time_interval))

        # Then we take the disjunction over each of these formulas
        return STLTree(self_until_kprime, "or", [0 for i in range(len(self_until_kprime))])

    def since(self, other, k1, k2):
        """
        Return a new :class:`.STLTree` :math:`\\varphi_{new}` which ensures that
        this formula (:math:`\\varphi`) holds for all time steps from the time step
        when the given formula :math:`\\varphi_{other}` holds at least one time step
        between :math:`k_1` and :math:`k_2`, until the current time step

        .. math::

            \\varphi_{new} = \\varphi S_{[k_1,k_2]}(\\varphi_{other})

        :param other:   A :class:`.STLFormula` representing :math:`\\varphi_{other`
        :param k1:  An integer representing the time :math:`k_1`
        :param k2:  An integer representing the time :math:`k_2`

        :return: An :class:`.STLTree` representing :math:`\\varphi_{new}`
        """
        # For every candidate switching time (k_prime), construct a subformula
        # representing 'self' holding since k_prime, at which point 'other' holds.
        self_since_kprime = []

        for k_prime in range(-k2, -k1+1):
            time_interval = [k for k in range(k_prime, 0)] + [0]
            subformula_list = [other] + [self for sf in range(k_prime+1, 0+1)]

            self_since_kprime.append(STLTree(subformula_list, "and", time_interval))

        # Then we take the disjunction over each of these formulas
        return STLTree(self_since_kprime, "or", [0 for i in range(len(self_since_kprime))])


class STLTree(STLFormula):

    def __init__(self, subformula_list, combination_type, timesteps, name=None):
        # Record the dimension of the signal this formula is defined over
        self.d = subformula_list[0].d

        # Run some type check on the inputs
        assert (combination_type == "and") or (combination_type == "or"), "Invalid combination type"
        assert isinstance(subformula_list, list), "subformula_list must be a list of STLTree or LinearPredicate objects"
        assert isinstance(timesteps, list), "timesteps must be a list of integers"
        assert len(timesteps) == len(subformula_list), "a timestep must be provided for each subformula"
        for formula in subformula_list:
            assert isinstance(formula, STLFormula), "subformula_list must be a list of STLTree or LinearPredicate objects"
            assert formula.d == self.d, "all subformulas must be defined over same dimension of signal"
        for k in timesteps:
            assert isinstance(k, int), "each timestep must be an integer"

        # Simply save the input arguments. We will parse these recursively later on to
        # determine, for example, the formula robustness.
        self.subformula_list = subformula_list
        self.combination_type = combination_type
        self.timesteps = timesteps

        # Save the given name for pretty printing
        self.name = name

        self.active = True
        self.is_parent_of_sub_spec = False

    def deactivate(self):
        self.active = False
        for sub_formula in self.subformula_list:
            sub_formula.deactivate()

    def negation(self):
        raise NotImplementedError("Only formulas in positive normal form are supported at this time")

    def robustness(self, y, k):
        if not self.active:
            return None

        # Time domain of the signal [0, K]
        K = y.shape[1] - 1

        # Determine the robustness of the subformulas in the time horizon
        sub_rhos = []
        for i, formula in enumerate(self.subformula_list):
            k_prime = k + self.timesteps[i]
            if 0 <= k_prime <= K:
                new_rho = formula.robustness(y, k_prime)
                if new_rho is not None:
                    sub_rhos.append(new_rho)

        if not sub_rhos:
            return None

        return min(sub_rhos) if self.combination_type == "and" else max(sub_rhos)

    def robustness_traced(self, y, k):
        if not self.active:
            return None, None, None

        # Time domain of the signal [0, K]
        K = y.shape[1] - 1

        # Determine the robustness of the subformulas in the time horizon
        sub_rhos = []
        pred_names = []
        time_steps = []
        for i, formula in enumerate(self.subformula_list):
            k_prime = k + self.timesteps[i]
            if 0 <= k_prime <= K:
                new_rho, pred_name, time_step = formula.robustness_traced(y, k_prime)
                if new_rho is not None:
                    sub_rhos.append(new_rho)
                    pred_names.append(pred_name)
                    time_steps.append(time_step)

        if not sub_rhos:
            return None, None, None

        if self.combination_type == "and":
            index_min = min(range(len(sub_rhos)), key=sub_rhos.__getitem__)
            return sub_rhos[index_min], pred_names[index_min], time_steps[index_min]
        else:
            index_max = max(range(len(sub_rhos)), key=sub_rhos.__getitem__)
            return sub_rhos[index_max], pred_names[index_max], time_steps[index_max]

    def is_predicate(self):
        return False

    def simplify(self):
        """
        Modify this formula to reduce the depth of the formula tree while preserving
        logical equivalence.

        A shallower formula tree can result in a more efficient binary encoding in some
        cases.
        """
        mod = True
        while mod:
            # Just keep trying to flatten until we don't get any improvement
            mod = self.flatten(self)

    def flatten(self, formula):
        """
        Reduce the depth of the given :class:`STLFormula` by combining adjacent
        layers with the same logical operation. This preserves the meaning of the
        formula, since, for example,

        ..math::

            (a \land b) \land (c \land d) = a \land b \land c \land d

        :param formula: The formula to modify

        :return made_modification: boolean flag indicating whether the formula was changed.l

        """
        made_modification = False

        for subformula in formula.subformula_list:
            if subformula.is_predicate():
                pass
            else:
                if formula.combination_type == subformula.combination_type:
                    # Remove the subformula
                    i = formula.subformula_list.index(subformula)
                    formula.subformula_list.pop(i)
                    st = formula.timesteps.pop(i)

                    # Add all the subformula's subformulas instead
                    formula.subformula_list += subformula.subformula_list
                    formula.timesteps += [k+st for k in subformula.timesteps]
                    made_modification = True

                made_modification = self.flatten(subformula) or made_modification

        return made_modification

    def __str__(self):
        """
        Return a string representing this formula. This string displays
        the tree structure of the formula, where each node represents either
        a conjuction or disjuction of subformulas, and leaves are state formulas.
        """
        tree = Tree()
        root = tree.create_node(self.combination_type)

        for subformula in self.subformula_list:
            self._add_subformula_to_tree(tree, root, subformula)

        return tree.__str__()

    def _add_subformula_to_tree(self, tree, root, formula):
        """
        Helper function for recursively parsing subformulas to create
        a Tree object for visualizing this formula.
        """
        if formula.is_predicate():
            tree.create_node(formula.__str__(), parent=root)
        else:
            new_node = tree.create_node(formula.combination_type, parent=root)
            for subformula in formula.subformula_list:
                self._add_subformula_to_tree(tree, new_node, subformula)
