""" This file is based on predicate.py from STLpy package"""
from abc import abstractmethod
from typing import Dict, Callable

import numpy as np
import numpy.typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.formula import STLFormula


class LinearPredicate(STLFormula):
    def __init__(self, config: Configuration,
                 a_static: npt.NDArray = None,
                 b_static: npt.NDArray = None,
                 a_func: Callable = None,
                 b_func: Callable = None,
                 nu: float = 1.0,
                 constraint_relation: ConstraintRelation = ConstraintRelation.GREATER_THAN,
                 name=None, y_dim: int = 3):

        # Check that one of the two is provided (a_static or a_func, b_static or b_func)
        assert (a_static is not None and a_func is None) or (a_func is not None and a_static is None), "Provide one!"
        assert (b_static is not None and b_func is None) or (b_func is not None and b_static is None), "Provide one!"

        self.K = config.planning.planning_horizon
        self.d = y_dim

        self.a_shape = (y_dim, self.K + 1)
        self.b_shape = (1, self.K + 1)

        self.a = np.zeros(self.a_shape)
        self.b = np.zeros(self.b_shape)

        self._a_func = a_func
        self._b_func = b_func

        self._alpha = constraint_relation.value / nu
        self._epsilon = 0.0

        self.name = name

        self.active = True
        self.is_parent_of_sub_spec = False

        self._config = config

        self._a_is_static = self.set_static_a(a_static)
        self._b_is_static = self.set_static_b(b_static)

        self.evaluated_time_steps = set()

    def deactivate(self):
        self.active = False

    def negation(self):
        # Check if predicate is to be negated multiple times
        assert 'not' not in self.name, "Negated predicate is to be negated again. This is prohibited!"

        if self.name is not None:
            self.name = self.name + "_NOT"

        self._alpha *= -1.0
        self._epsilon = self._config.rulebook.epsilon

        # In case parameters are static (beyond that, it has no other effect)
        self.a = -1.0 * self.a
        self.b = -1.0 * self.b + self._config.rulebook.epsilon

        return self

    def update(self, data: Dict):
        if self.active:
            if not self._a_is_static:
                self.update_a(self.get_a(data))
            if not self._b_is_static:
                self.update_b(self.get_b(data))

    def update_a(self, a: npt.NDArray):
        self.a = self._alpha * a
        assert self.a.shape == self.a_shape, 'a is defined incorrectly.'

    def update_b(self, b: npt.NDArray):
        self.b = self._alpha * b + self._epsilon
        assert self.b.shape == self.b_shape, 'b is defined incorrectly.'

    def set_static_a(self, a: npt.NDArray) -> bool:
        if a is not None:
            # Repeat a vector K+1 times
            a = np.array(a).reshape((-1, 1))
            self.update_a(np.tile(a, (1, self.K + 1)))
            return True
        else:
            return False

    def set_static_b(self, b: npt.NDArray) -> bool:
        if b is not None:
            # Repeat b vector K+1 times
            b = np.array(b).reshape((-1, 1))
            self.update_b(np.tile(b, (1, self.K + 1)))
            return True
        else:
            return False

    @abstractmethod
    def get_a(self, data: Dict) -> npt.NDArray:
        pass

    @abstractmethod
    def get_b(self, data: Dict) -> npt.NDArray:
        pass

    def robustness(self, y, k):
        return (self.a[:, k].T @ y[:, k] - self.b[:, k]).item()

    def robustness_traced(self, y, k):
        self.evaluated_time_steps.add(k)
        return (self.a[:, k].T @ y[:, k] - self.b[:, k]).item(), self.name, k

    def robustness_over_time_horizon(self, y) -> npt.NDArray:
        return np.diagonal(self.a.T @ y) - self.b

    def is_predicate(self):
        return True

    def __str__(self):
        if self.name is None:
            return "{ Predicate %s*y + >= %s }" % (self.a, self.b)
        else:
            return "{ Predicate " + self.name + " }"
