from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Dict, List, Union, Tuple

import networkx as nx
from networkx import DiGraph

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.linear_predicate import LinearPredicate
from micp_planner.rulebook.base_classes.piecewise_linear_predicate import PiecewiseLinearPredicate

ROBUSTNESS_INF = 1e+5


class Node:
    def __init__(self, k: int, sub_formula: STLTree):
        self.k = k
        self.sub_formula = sub_formula
        self.predicate = None
        self.combination_type = sub_formula.combination_type if hasattr(sub_formula, 'combination_type') else None
        self.name = sub_formula.name
        self.is_parent_of_sub_spec = sub_formula.is_parent_of_sub_spec


class Specification(ABC):
    """
    Specification
    """

    def __init__(self, name: str, config: Configuration):
        self.name = name
        self.config = config

        self.stl_tree = None
        self.K = config.planning.planning_horizon
        self.predicates = {}
        self.deactivatable_sub_specs = {}
        self.sub_specs = {}

    @abstractmethod
    def get_stl_tree(self) -> STLTree:
        pass

    def mark_for_deactivation(self, sub_specification: STLTree, name: str):
        sub_specification.name = name
        sub_specification.is_parent_of_sub_spec = True
        self.deactivatable_sub_specs[name] = sub_specification

    def deactivate_sub_specifications(self, data: Data):
        # Deactivate own sub-specifications
        self.deactivate_sub_specs(data)

        # Deactivate sub-specifications of sub-specifications
        for sub_spec in self.sub_specs.values():
            sub_spec.deactivate_sub_specifications(data)

    def deactivate_sub_specs(self, data: Data):
        pass

    def get(self) -> Specification:
        self.stl_tree = self.get_stl_tree()
        self.predicates = self.extract_predicates()
        return self

    def extract_predicates(self) -> Dict:
        # Recursive function to extract predicates from spec
        def extract_preds(spec) -> List:
            preds = []
            if spec.is_predicate():
                preds.append(spec)
            else:
                for sub_spec in spec.subformula_list:
                    preds += extract_preds(sub_spec)
            return preds

        preds = set(extract_preds(self.stl_tree))

        # Put predicates into a dictionary
        predicates = {}
        for predicate in preds:
            predicates[predicate.name] = predicate

        return predicates

    def get_deactivatable_sub_spec(self, name: str) -> Union[STLTree, None]:
        sub_spec = self.deactivatable_sub_specs.get(name)
        if sub_spec is not None:
            return sub_spec

        for spec in self.sub_specs.values():
            sub_spec = spec.get_deactivatable_sub_spec(name)
            if sub_spec:
                return sub_spec

        return None

    def deactivate_sub_specification(self, name: str):
        self.deactivatable_sub_specs[name].deactivate()

    def graph(self, k_init: int) -> Tuple[DiGraph, Node]:
        assert self.stl_tree is not None, 'STL tree was not yet calculated!'

        def add_subformulas(formula: STLTree, node: Node) -> bool:
            if isinstance(formula, LinearPredicate) or isinstance(formula, PiecewiseLinearPredicate):
                node.predicate = formula
                return True

            else:
                child_nodes = []
                for i, subformula in enumerate(formula.subformula_list):
                    k_child = node.k + formula.timesteps[i]
                    if 0 <= k_child <= self.K:
                        child_node = Node(k_child, subformula)
                        has_children = add_subformulas(subformula, child_node)
                        if has_children:
                            child_nodes.append(child_node)

                if len(child_nodes) == 0:
                    return False
                else:
                    for child in child_nodes:
                        graph.add_edge(node, child)
                    return True

        graph = nx.DiGraph()

        # Add parent node
        parent_node = Node(k_init, self.stl_tree)
        graph.add_node(parent_node)

        _ = add_subformulas(self.stl_tree, parent_node)

        return graph, parent_node

    def robustness(self, y, k):
        rho = self.stl_tree.robustness(y, k)

        # If the robustness calculation returned None, we return ROBUSTNESS_INF == TRUE
        if rho is None:
            return ROBUSTNESS_INF
        else:
            return rho
