from typing import Callable

import numpy as np
from numpy import typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.linear_predicate import LinearPredicate


class KEEPS_SAFE_DISTANCE(LinearPredicate):
    def __init__(self, config: Configuration, name: str, a_b_func: Callable[[Data], Prediction]):
        super().__init__(config, a_func=a_b_func, b_func=a_b_func,
                         nu=config.rulebook.nu_keeps_safe_distance,
                         constraint_relation=ConstraintRelation.LOWER_THAN,
                         name=name + "KEEPS_SAFE_DISTANCE")

    def get_a(self, data: Data) -> npt.NDArray:
        prediction = self._a_func(data)

        a = np.zeros((3, self.K + 1))
        for k in range(self.K + 1):
            a[0, k] = 1.0
            test = prediction.safe_dist_params[k]['m']
            a[1, k] = prediction.safe_dist_params[k]['m']
        return a

    def get_b(self, data: Data) -> npt.NDArray:
        ego_length_half = self._config.vehicle.length / 2.0  # Approximation with half ego length
        prediction = self._b_func(data)

        b = np.zeros((1, self.K + 1))
        for k in range(self.K + 1):
            b[0, k] = prediction.s_min[k] - prediction.safe_dist_params[k]['b'] - ego_length_half
        return b
