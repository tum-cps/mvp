from typing import Callable

import numpy as np
from numpy import typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.linear_predicate import LinearPredicate


class IN_FRONT_OF(LinearPredicate):
    def __init__(self, config: Configuration, name: str, b_func: Callable[[Data], Prediction]):
        super().__init__(config, a_static=[1.0, 0.0, 0.0], b_func=b_func,
                         nu=config.rulebook.nu_in_front_of,
                         constraint_relation=ConstraintRelation.LOWER_THAN,
                         name=name + "IN_FRONT_OF")

    def get_a(self, data: Data) -> npt.NDArray:
        pass

    def get_b(self, data: Data) -> npt.NDArray:
        prediction = self._b_func(data)
        ego_length_half = self._config.vehicle.length / 2.0

        b = np.zeros((1, self.K + 1))
        for k in range(self.K + 1):
            b[0, k] = prediction.s_min[k] + ego_length_half
        return b
