from typing import Callable, List, Tuple

import numpy as np
from numpy import typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.piecewise_linear_predicate import PiecewiseLinearPredicate


def safe_distance(obstacle_velocity: float, ego_velocity: float, config: Configuration) -> float:
    # Check if the min ego velocity is >= 0 -> Otherwise the approximation is not valid
    assert config.vehicle.v_min >= 0, "The minimum velocity of the ego is < 0. " \
                                      "The safe distance approximation does not hold for this case!"

    # Get parameters
    t_react = config.rulebook.t_react
    a_e_min = config.vehicle.a_min
    a_o_min = config.rulebook.a_min_obs
    v_o = obstacle_velocity
    return v_o ** 2 / (-2 * abs(a_o_min)) - ego_velocity ** 2 / (-2 * abs(a_e_min)) + t_react * ego_velocity


def safe_distance_prec_p(prediction, k: int, s_ego, v_ego, config: Configuration) -> float:
    ego_length_half = config.vehicle.length / 2.0
    return prediction.s_min[k] - (s_ego + ego_length_half) - safe_distance(prediction.velocity[k], v_ego, config)


def determineSubspaces(config: Configuration, nof_subspaces: int = 0) -> List[List[Tuple[npt.NDArray, npt.NDArray]]]:
    v_min = config.vehicle.v_min - 0.001
    v_max = config.vehicle.v_max + 0.001

    delta_v = (v_max - v_min) / nof_subspaces

    subspaces = []
    for i in range(nof_subspaces):
        v0 = v_min + i * delta_v
        v1 = v_min + (i + 1) * delta_v
        new_subspace = [(np.array([0, 1, 0]), np.array([v0])),    # v >= v0
                        (np.array([0, -1, 0]), np.array([-v1]))]  # v <= v1
        subspaces.append(new_subspace)
    return subspaces


class KEEPS_SAFE_DISTANCE_PWL(PiecewiseLinearPredicate):
    def __init__(self, config: Configuration, name: str, a_b_func: Callable[[Data], Prediction]):
        subspaces = determineSubspaces(config, nof_subspaces=2)  # TODO: Also change value in prediction class!!
        self.config = config

        super().__init__(config, a_func=a_b_func, b_func=a_b_func, subspaces=subspaces,
                         nu=config.rulebook.nu_keeps_safe_distance,
                         constraint_relation=ConstraintRelation.LOWER_THAN,
                         name=name + "KEEPS_SAFE_DISTANCE_PWL")

    def get_a_sub(self, data: Data, sub_id: int) -> npt.NDArray:
        prediction = self._a_func(data)
        subspace = self.subspaces[sub_id]

        # Sampling points
        samples = [[0, subspace[0][1]],     # v_min
                   [0, -subspace[1][1]],    # v_max
                   [10, -subspace[1][1]]]   # s can be random here

        a = np.zeros((3, self.K + 1))
        for k in range(self.K + 1):
            # Evaluate non-linear p-function at sampling points
            p = []
            for sample in samples:
                p.append(safe_distance_prec_p(prediction, k, sample[0], sample[1], self.config))

            a[0, k] = 1.0
            a[1, k] = prediction.safe_dist_params_multi[k][sub_id]['m']
        return a

    def get_b_sub(self, data: Data, sub_id: int) -> npt.NDArray:
        prediction = self._b_func(data)
        subspace = self.subspaces[sub_id]

        # Sampling points
        samples = [[0, subspace[0][1]],     # v_min
                   [0, -subspace[1][1]],    # v_max
                   [10, -subspace[1][1]]]   # s can be random here

        b = np.zeros((1, self.K + 1))
        for k in range(self.K + 1):
            # Evaluate non-linear p-function at sampling points
            p = []
            for sample in samples:
                p.append(safe_distance_prec_p(prediction, k, sample[0], sample[1], self.config))

            ego_length_half = self.config.vehicle.length / 2.0
            b[0, k] = prediction.s_min[k] - prediction.safe_dist_params_multi[k][sub_id]['b'] - ego_length_half
        return b
