from typing import Callable

import numpy as np
from numpy import typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.linear_predicate import LinearPredicate


class ACC_LOWER_THAN(LinearPredicate):
    def __init__(self, config: Configuration, name: str, b_static: npt.NDArray = None,
                 b_func: Callable[[Data], float] = None):

        super().__init__(config, a_static=[0.0, 0.0, 1.0], b_static=b_static, b_func=b_func,
                         nu=config.rulebook.nu_acc_lower_than,
                         constraint_relation=ConstraintRelation.LOWER_THAN,
                         name=name + "ACC_LOWER_THAN")

    def get_a(self, data: Data) -> npt.NDArray:
        pass

    def get_b(self, data: Data) -> npt.NDArray:
        b = self._b_func(data)
        return np.full(self.b_shape, b)
