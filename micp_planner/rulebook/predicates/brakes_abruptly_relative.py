from typing import Callable

import numpy as np
from numpy import typing as npt

from micp_planner.common.configuration import Configuration
from micp_planner.common.data import Data
from micp_planner.common.prediction import Prediction
from micp_planner.common.utils import ConstraintRelation
from micp_planner.rulebook.base_classes.linear_predicate import LinearPredicate


class BRAKES_ABRUPTLY_RELATIVE(LinearPredicate):
    def __init__(self, config: Configuration, name: str, b_func: Callable[[Data], Prediction]):
        super().__init__(config, a_static=[0.0, 0.0, 1.0], b_func=b_func,
                         nu=config.rulebook.nu_brakes_abruptly_relative,
                         constraint_relation=ConstraintRelation.LOWER_THAN,
                         name=name + "BRAKES_ABRUPTLY_RELATIVE")

    def get_a(self, data: Data) -> npt.NDArray:
        pass

    def get_b(self, data: Data) -> npt.NDArray:
        u_abr = self._config.rulebook.u_abr
        prediction = self._b_func(data)

        b = np.zeros((1, self.K + 1))
        for k in range(self.K + 1):
            b[0, k] = prediction.acceleration[k] + u_abr
        return b
