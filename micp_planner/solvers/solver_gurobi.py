# This file is based on https://github.com/vincekurtz/stlpy/

from typing import List, Tuple

import gurobipy as gp
import numpy.typing as npt
import portion as Int
from commonroad_reach.pycrreach import ReachPolygon
from gurobipy import *
from networkx import DiGraph

from micp_planner.common.vehicle_model import VehicleModel
from micp_planner.rulebook.base_classes.formula import STLTree
from micp_planner.rulebook.base_classes.linear_predicate import LinearPredicate
from micp_planner.rulebook.base_classes.piecewise_linear_predicate import PiecewiseLinearPredicate
from micp_planner.rulebook.base_classes.specification import Node, Specification
from micp_planner.rulebook.rulebook import Rulebook


class PredicateConstraint:
    def __init__(self, constraint: Constr, constraint_name: str, predicate: LinearPredicate, k: int):
        self.constraint = constraint
        self.constraint_name = constraint_name
        self.predicate = predicate
        self.predicate_name = predicate.name
        self.k = k

    def update(self, model: gp.Model, rulebook: Rulebook):
        self.constraint = model.getConstrByName(self.constraint_name)
        self.predicate = rulebook.get_predicate_by_name(self.predicate_name)


class PWLPredicateConstraint:
    def __init__(self, constraint: Constr, constraint_name: str, predicate: LinearPredicate, k: int,
                 subspace_id: int, z: Var, z_name: str, rho: Var, rho_name: str, delta: Var, delta_name: str):
        self.constraint = constraint
        self.constraint_name = constraint_name
        self.predicate = predicate
        self.predicate_name = predicate.name
        self.k = k
        self.subspace_id = subspace_id
        self.z = z
        self.z_name = z_name
        self.rho = rho
        self.rho_name = rho_name
        self.delta = delta
        self.delta_name = delta_name

    def update(self, model: gp.Model, rulebook: Rulebook):
        for constr in model.getGenConstrs():
            if constr.GenConstrName == self.constraint_name:
                self.constraint = constr

        self.predicate = rulebook.get_predicate_by_name(self.predicate_name)
        self.z = model.getVarByName(self.z_name)
        self.rho = model.getVarByName(self.rho_name)
        self.delta = model.getVarByName(self.delta_name)


class SubformulaParent:
    def __init__(self, z: Var, z_name: str, subformula: STLTree, parent_combination_type: str):
        self.z = z
        self.z_name = z_name
        self.subformula = subformula
        self.subformula_name = subformula.name
        self.parent_combination_type = parent_combination_type

    def update(self, model: gp.Model, rulebook: Rulebook):
        self.z = model.getVarByName(self.z_name)
        self.subformula = rulebook.get_deactivatable_sub_spec_by_name(self.subformula_name)


class SolverGurobi:
    def __init__(self, name: str, vehicle_model: VehicleModel, K: int, nof_reachable_pos_intervals: int, nof_kinematic_speed_limit_segments: int, verbose=True,
                 M: float = 1000, RHO_INF: float = 1e+10, presolve=True):
        # Set member variables
        self.name = name
        self.vm = vehicle_model
        self.K = K
        self.nof_reachable_pos_intervals = nof_reachable_pos_intervals
        self.nof_kinematic_speed_limit_segments = nof_kinematic_speed_limit_segments
        self.M = M
        self.RHO_INF = RHO_INF
        self.presolve = presolve
        self.verbose = verbose

        # Set up gurobi model
        self.model = gp.Model(name)

        # Decision variables for state, control variables, robustness variables, reachability variables,
        # kinematic speed limit variables
        self.x = None
        self.u = None
        self.rho = []
        self.reachable_pos_intervals = None
        self.reachable_vel_intervals = None
        self.reachable_pos_intervals_indicators = None
        self.kinematic_speed_limit_pos_intervals = None
        self.kinematic_speed_limit_vel_intervals = None
        self.kinematic_speed_limit_pos_intervals_indicators = None

        # Names of encoded specifications in this solver
        self.spec_names = []

        # Flag indicating whether objective is set
        self.objective_is_set = False

        # Flags indicating whether constraints were set
        self.initial_state_constraints_are_set = False
        self.system_dynamics_constraints_are_set = False
        self.state_bounds_lower_constraints_are_set = False
        self.state_bounds_upper_constraints_are_set = False
        self.control_bounds_lower_constraints_are_set = False
        self.control_bounds_upper_constraints_are_set = False
        self.reachability_constraints_are_set = False
        self.kinematic_speed_limit_constraints_are_set = False

        # Reference to constraints which will be updated dynamically
        self.predicate_constraints = []
        self.pwl_predicate_constraints = []
        self.subformula_parents = []

        # Delta optimization variables predicate mapping
        self.subspaces_deltas = {}

        # Number of integer and binary variables
        self.nof_rho_variables = 0
        self.nof_integer_variables = 0
        self.nof_binary_variables = 0

        # Number of binary, integer and predicate constraints
        self.nof_binary_constraints = 0
        self.nof_integer_constraints = 0
        self.nof_predicate_constraints = 0
        self.nof_pwl_predicate_constraints = 0

    def updateRankIndependentProperties(self, x_0: npt.NDArray, reachable_sets: List[List[ReachPolygon]],
                                        kinematic_speed_limit_segments: List[List]):
        self.updateInitialState(x_0)
        self.updateReachabilityConstraints(reachable_sets)
        self.updateKinematicSpeedLimitConstraints(kinematic_speed_limit_segments)
        self.updatePredicateConstraints()
        self.updatePWLPredicateConstraints()
        self.deactivateIntegers()

    def addDecisionVariables(self):
        self.x = self.model.addMVar((self.vm.n, self.K + 1), lb=-1 * GRB.INFINITY, name='x')
        self.u = self.model.addMVar((self.vm.m, self.K + 1), lb=-1 * GRB.INFINITY, name='u')
        self.reachable_pos_intervals = self.model.addMVar((self.nof_reachable_pos_intervals, self.K),
                                                          lb=-1 * GRB.INFINITY,
                                                          ub=GRB.INFINITY, name='reachable_pos_intervals')
        self.reachable_vel_intervals = self.model.addMVar((self.nof_reachable_pos_intervals, self.K),
                                                          lb=-1 * GRB.INFINITY,
                                                          ub=GRB.INFINITY, name='reachable_vel_intervals')
        self.reachable_pos_intervals_indicators = self.model.addMVar((self.nof_reachable_pos_intervals, self.K),
                                                                     vtype=GRB.BINARY,
                                                                     name='reachable_pos_intervals_indicators')
        self.kinematic_speed_limit_pos_intervals = self.model.addMVar((self.nof_kinematic_speed_limit_segments, self.K),
                                                          lb=-1 * GRB.INFINITY,
                                                          ub=GRB.INFINITY, name='kinematic_speed_limit_pos_intervals')
        self.kinematic_speed_limit_vel_intervals = self.model.addMVar((self.nof_kinematic_speed_limit_segments, self.K),
                                                          lb=-1 * GRB.INFINITY,
                                                          ub=GRB.INFINITY, name='kinematic_speed_limit_vel_intervals')
        self.kinematic_speed_limit_pos_intervals_indicators = self.model.addMVar((self.nof_kinematic_speed_limit_segments, self.K),
                                                                     vtype=GRB.BINARY,
                                                                     name='kinematic_speed_limit_pos_intervals_indicators')

    def updateInitialState(self, x0: npt.NDArray[float]):
        self.x[:, 0].lb = x0
        self.x[:, 0].ub = x0
        self.initial_state_constraints_are_set = True

    def setSystemDynamicsConstraints(self):
        if not self.system_dynamics_constraints_are_set:
            self.model.addConstrs(
                (self.x[:, k + 1] == self.vm.A @ self.x[:, k] + self.vm.B @ self.u[:, k] for k in range(self.K)),
                name='SystemDynamicsConstraints')
            self.system_dynamics_constraints_are_set = True
        else:
            print("SystemDynamicConstraints were already set. No update was performed.")

    def setStateBounds(self, x_min: npt.NDArray[float], x_max: npt.NDArray[float]):
        if not self.state_bounds_lower_constraints_are_set:
            for k in range(1, self.K + 1):
                self.x[:, k].lb = x_min
            self.state_bounds_lower_constraints_are_set = True
        else:
            print("StateBoundsLowerConstraints were already set. No update was performed.")
        if not self.state_bounds_upper_constraints_are_set:
            for k in range(1, self.K + 1):
                self.x[:, k].ub = x_max
            self.state_bounds_upper_constraints_are_set = True
        else:
            print("StateBoundsUpperConstraints were already set. No update was performed.")

    def setControlBounds(self, u_min: npt.NDArray[float], u_max: npt.NDArray[float]):
        if not self.control_bounds_lower_constraints_are_set:
            for k in range(self.K + 1):
                self.u[:, k].lb = u_min
            self.control_bounds_lower_constraints_are_set = True
        else:
            print("ControlBoundsLowerConstraints were already set. No update was performed.")
        if not self.control_bounds_upper_constraints_are_set:
            for k in range(self.K + 1):
                self.u[:, k].ub = u_max
            self.control_bounds_upper_constraints_are_set = True
        else:
            print("ControlBoundsUpperConstraints were already set. No update was performed.")

    def setKinematicSpeedLimitConstraints(self):
        if not self.kinematic_speed_limit_constraints_are_set:
            for k in range(1, self.K + 1):
                for i in range(self.nof_kinematic_speed_limit_segments):
                    self.model.addConstr((self.kinematic_speed_limit_pos_intervals_indicators[i, k - 1].item() == 1) >> (
                                self.x[0, k] == self.kinematic_speed_limit_pos_intervals[i, k - 1]),
                                         name="KinematicSpeedLimitPositionConstraints[" + str(k) + "][" + str(i) + "]")
                    self.model.addConstr((self.kinematic_speed_limit_pos_intervals_indicators[i, k - 1].item() == 1) >> (
                            self.x[1, k] == self.kinematic_speed_limit_vel_intervals[i, k - 1]),
                                         name="KinematicSpeedLimitVelocityConstraints[" + str(k) + "][" + str(i) + "]")
                self.model.addConstr(gp.quicksum(self.kinematic_speed_limit_pos_intervals_indicators[:, k - 1]) == 1,
                                     name="KinematicSpeedLimitIntegerConstraints[" + str(k) + "]")
            self.kinematic_speed_limit_constraints_are_set = True
        else:
            print("KinematicSpeedLimitConstraints were already set. No update was performed.")

    def updateKinematicSpeedLimitConstraints(self, kinematic_speed_limits: List[List]):
        if self.kinematic_speed_limit_constraints_are_set:
            for k in range(1, self.K + 1):
                for i in range(self.nof_kinematic_speed_limit_segments):
                    kinematic_speed_limit_pos_interval = self.kinematic_speed_limit_pos_intervals[i, k - 1]
                    kinematic_speed_limit_vel_interval = self.kinematic_speed_limit_vel_intervals[i, k - 1]
                    kinematic_speed_limit_pos_interval.lb = kinematic_speed_limits[i][0]  # s_min
                    kinematic_speed_limit_pos_interval.ub = kinematic_speed_limits[i][1]  # s_max
                    kinematic_speed_limit_vel_interval.lb = -gp.GRB.INFINITY              # v_min
                    kinematic_speed_limit_vel_interval.ub = kinematic_speed_limits[i][2]  # v_max
        else:
            print("KinematicSpeedLimitConstraints were not set yet. No update was performed.")

    def setReachabilityConstraints(self):
        if not self.reachability_constraints_are_set:
            for k in range(1, self.K + 1):
                for i in range(self.nof_reachable_pos_intervals):
                    self.model.addConstr((self.reachable_pos_intervals_indicators[i, k - 1].item() == 1) >> (
                                self.x[0, k] == self.reachable_pos_intervals[i, k - 1]),
                                         name="ReachabilityPositionConstraints[" + str(k) + "][" + str(i) + "]")
                    self.model.addConstr((self.reachable_pos_intervals_indicators[i, k - 1].item() == 1) >> (
                            self.x[1, k] == self.reachable_vel_intervals[i, k - 1]),
                                         name="ReachabilityVelocityConstraints[" + str(k) + "][" + str(i) + "]")
                self.model.addConstr(gp.quicksum(self.reachable_pos_intervals_indicators[:, k - 1]) == 1,
                                     name="ReachabilityIntegerConstraints[" + str(k) + "]")
            self.reachability_constraints_are_set = True
        else:
            print("ReachabilityConstraints were already set. No update was performed.")

    def updateReachabilityConstraints(self, reachable_set: List[List[Int.Interval]]):
        # Reset indicators:
        for indicator in self.reachable_pos_intervals_indicators:
            indicator.lb = 0
            indicator.ub = 1

        if self.reachability_constraints_are_set:
            for k in range(1, self.K + 1):
                for i in range(self.nof_reachable_pos_intervals):
                    if i < len(reachable_set[k]):
                        self.reachable_pos_intervals[i, k - 1].lb = reachable_set[k][i].p_min
                        self.reachable_pos_intervals[i, k - 1].ub = reachable_set[k][i].p_max
                        self.reachable_vel_intervals[i, k - 1].lb = reachable_set[k][i].v_min
                        self.reachable_vel_intervals[i, k - 1].ub = reachable_set[k][i].v_max
                    else:
                        # deactivate dynamic constraints which correspond to nonexistent reachable position interval
                        self.reachable_pos_intervals_indicators[i, k - 1].lb = 0
                        self.reachable_pos_intervals_indicators[i, k - 1].ub = 0
                        self.reachable_pos_intervals[i, k - 1].lb = -GRB.INFINITY
                        self.reachable_pos_intervals[i, k - 1].ub = GRB.INFINITY
                        self.reachable_vel_intervals[i, k - 1].lb = -GRB.INFINITY
                        self.reachable_vel_intervals[i, k - 1].ub = GRB.INFINITY
        else:
            print("ReachabilityConstraints were not set yet. No update was performed.")

    def updateRobustnessConstraints(self, rho_lbs: List[float]):
        # By fixing the optimization variable rho, we generate a lower bound to the robustness constraint ax - b > rho
        for rho, rho_lb in zip(self.rho, rho_lbs):
            rho.lb = rho_lb
            rho.ub = rho_lb

    def setQuadraticCostFunction(self, Q: npt.NDArray[float], R: npt.NDArray[float]):
        cost = 0.0
        for k in range(self.K + 1):
            cost += self.x[:, k] @ Q @ self.x[:, k] + self.u[:, k] @ R @ self.u[:, k]
        self.model.setObjective(cost, GRB.MINIMIZE)
        self.objective_is_set = True

    def setQuadraticNegativeRobustnessCostFunction(self, Q: npt.NDArray[float], R: npt.NDArray[float],
                                                   w_rho: List[float]):
        cost = 0.0
        for k in range(self.K + 1):
            cost += self.x[:, k] @ Q @ self.x[:, k] + self.u[:, k] @ R @ self.u[:, k]

        assert len(w_rho) == len(self.rho), 'Length of rho weights does not match the rho optimization variables!'
        for i, w in enumerate(w_rho):
            var_rho = self.model.addVar(lb=-float('inf'), name=f'var_rho[{i}]')
            self.model.addGenConstrMin(var_rho, [self.rho[i]], 0.0, name=f'VarRhoConstraint[{i}]')
            cost += -w * var_rho

        self.model.setObjective(cost, GRB.MINIMIZE)
        self.objective_is_set = True

    def setRobustnessCostFunction(self, rho: Var):
        self.model.setObjective(rho, GRB.MAXIMIZE)
        self.objective_is_set = True

    def solve(self, check_prerequisites: bool = False):
        # set solver options
        if not self.presolve:
            self.model.setParam('Presolve', 0)
        if not self.verbose:
            self.model.setParam('OutputFlag', 0)

        self.model.setParam('TimeLimit', 60)
        self.model.setParam('NumericFocus', 3)

        # check prerequisites
        if check_prerequisites:
            self.check_optimization_prerequisites()

        # Optimize model
        self.model.optimize()

        # Extract solution
        if self.model.status == GRB.OPTIMAL:
            if self.verbose:
                print("\nOptimal Solution Found!\n")
            x = self.x.X
            u = self.u.X
            obj_val = self.model.ObjVal

        else:
            if self.verbose:
                print(f"\nOptimization failed with status {self.model.status}.\n")
            x = None
            u = None
            obj_val = None

        return x, u, obj_val, self.model.Runtime

    def check_optimization_prerequisites(self):
        if not (self.initial_state_constraints_are_set
                and self.system_dynamics_constraints_are_set
                and self.state_bounds_lower_constraints_are_set
                and self.state_bounds_upper_constraints_are_set
                and self.control_bounds_lower_constraints_are_set
                and self.control_bounds_upper_constraints_are_set
                and self.reachability_constraints_are_set
                and self.kinematic_speed_limit_constraints_are_set):
            raise ValueError("Not all constraints were set yet.")
        if not self.objective_is_set:
            raise ValueError("Objective is not set yet.")

    def addSpecification(self, spec: Specification) -> Var:
        self.spec_names.append(spec.name)
        spec_graph, root_node = spec.graph(k_init=0)

        # Add optimization variable for rho
        rho_name = self.getRhoVariableName()
        rho = self.model.addVar(lb=-self.RHO_INF, ub=self.RHO_INF, name=rho_name)
        self.rho.append(rho)

        # Add integer optimization variable
        z_spec, z_name = self.addIntegerVariable(root_node)
        self.model.addConstr(z_spec == 1, name=self.getIntegerConstraintName())

        # Recursively add optimization variables and constraints for sub-formulas
        self.addSubformulaConstraints(root_node, z_spec, z_name, self.rho[-1], rho_name, spec_graph)

        return rho

    def addSubformulaConstraints(self, node: Node, z: Var, z_name: str, rho: Var, rho_name: str, spec_graph: DiGraph):
        # We're at the bottom of the tree, so add the big-M constraints
        if isinstance(node.sub_formula, LinearPredicate):
            k = node.k

            # Initialize linear expression and rhs for constraint
            A = LinExpr([0.0, 0.0, 0.0] + [-self.M, -1.0], self.x[:, k].tolist() + self.u[:, k].tolist() + [z] + [rho])
            rhs = 0.0 - self.M

            # Create a new predicate constraint, add it to the model, and append it to the list of predicate constraints
            pc_name = self.getPredicateConstraintName()
            pc = PredicateConstraint(self.model.addLConstr(A, GRB.GREATER_EQUAL, rhs, name=pc_name), pc_name, node.predicate, k)
            self.predicate_constraints.append(pc)

            # Add binary optimization variable and make constraint on z
            z_binary = self.model.addVar(vtype=GRB.BINARY, name=self.getBinaryVariableName())
            self.model.addConstr(z == z_binary, name=self.getIntegerConstraintName())

        elif isinstance(node.sub_formula, PiecewiseLinearPredicate):
            k = node.k
            for i, subspace in enumerate(node.sub_formula.subspaces):
                # Initialize linear expression and rhs for constraint
                A = LinExpr([0.0, 0.0, 0.0] + [-self.M, -1.0], self.x[:, k].tolist() + self.u[:, k].tolist() + [z] + [rho])
                rhs = 0.0 - self.M

                pwl_pc_name = self.getPWLPredicateConstraintName()
                constr = self.model.addLConstr(A, GRB.GREATER_EQUAL, rhs, name=pwl_pc_name)
                pwl_pc = PWLPredicateConstraint(constr, pwl_pc_name, node.predicate, k, i, None, None, None, None, None, None)
                self.pwl_predicate_constraints.append(pwl_pc)

                # Add binary optimization variable and make constraint on z
                z_binary = self.model.addVar(vtype=GRB.BINARY, name=self.getBinaryVariableName())
                self.model.addConstr(z == z_binary, name=self.getIntegerConstraintName())


        # We haven't reached the bottom of the tree, so keep adding boolean constraints recursively
        else:
            if node.combination_type == "and":
                for child_node in spec_graph.successors(node):
                    # Add optimization variable for z and make constraint
                    z_sub, z_sub_name = self.addIntegerVariable(child_node, 'and')
                    self.model.addConstr(z <= z_sub, name=self.getIntegerConstraintName())

                    self.addSubformulaConstraints(child_node, z_sub, z_sub_name, rho, rho_name, spec_graph)

            else:  # combination_type == "or":
                z_subs = []
                for child_node in spec_graph.successors(node):
                    # Add optimization variable for z
                    z_sub, z_sub_name = self.addIntegerVariable(child_node, 'or')
                    z_subs.append(z_sub)

                    self.addSubformulaConstraints(child_node, z_sub, z_sub_name, rho, rho_name, spec_graph)

                # Make constraint on all z_subs
                self.model.addConstr(z <= gp.quicksum(z_subs), name=self.getIntegerConstraintName())

    def addIntegerVariable(self, node, parent_combination_type: str = '') -> Tuple[Var, str]:
        z_name = self.getIntegerVariableName()
        z = self.model.addVar(vtype=GRB.CONTINUOUS, name=z_name)
        if node.sub_formula.is_parent_of_sub_spec:
            self.subformula_parents.append(SubformulaParent(z, z_name, node.sub_formula, parent_combination_type))
        return z, z_name

    def getIntegerConstraintName(self) -> str:
        self.nof_integer_constraints += 1
        return 'IntegerConstraints[' + str(self.nof_integer_constraints) + ']'

    def getPredicateConstraintName(self) -> str:
        self.nof_predicate_constraints += 1
        return 'ObstacleConstraints[' + str(self.nof_predicate_constraints) + ']'

    def getPWLPredicateConstraintName(self) -> str:
        self.nof_pwl_predicate_constraints += 1
        return 'PWLConstraint[' + str(self.nof_pwl_predicate_constraints) + ']'

    def getRhoVariableName(self) -> str:
        self.nof_rho_variables += 1
        return 'rho[' + str(self.nof_rho_variables) + ']'

    def getIntegerVariableName(self) -> str:
        self.nof_integer_variables += 1
        return 'z[' + str(self.nof_integer_variables) + ']'

    def getBinaryVariableName(self) -> str:
        self.nof_binary_variables += 1
        return 'bin[' + str(self.nof_binary_variables) + ']'

    def getStatistics(self):
        self.model.update()
        return [self.model.numVars, self.model.numIntVars, self.model.numConstrs, self.nof_integer_constraints]

    def updatePredicateConstraints(self):
        for constr in self.predicate_constraints:
            constraint = constr.constraint
            k = constr.k
            predicate = constr.predicate

            if predicate.active:
                self.model.chgCoeff(constraint, self.x[:, k].tolist()[0], predicate.a[0, k])
                self.model.chgCoeff(constraint, self.x[:, k].tolist()[1], predicate.a[1, k])
                self.model.chgCoeff(constraint, self.u[:, k].tolist()[0], predicate.a[2, k])
                constraint.rhs = predicate.b[0, k] - self.M
            else:
                constraint.rhs = -GRB.INFINITY

    def updatePWLPredicateConstraints(self):
        for constr in self.pwl_predicate_constraints:
            constraint = constr.constraint
            k = constr.k
            predicate = constr.predicate

            if predicate.active:
                i = constr.subspace_id

                self.model.chgCoeff(constraint, self.x[:, k].tolist()[0], predicate.a_sub[i][0, k])
                self.model.chgCoeff(constraint, self.x[:, k].tolist()[1], predicate.a_sub[i][1, k])
                self.model.chgCoeff(constraint, self.u[:, k].tolist()[0], predicate.a_sub[i][2, k])
                constraint.rhs = predicate.b_sub[i][0, k] - self.M
            else:
                constraint.rhs = -GRB.INFINITY

    def deactivateIntegers(self):
        for sp in self.subformula_parents:
            if sp.subformula.active:
                sp.z.lb = 0.0
                sp.z.ub = 1.0
            else:  # sp is inactive
                if sp.parent_combination_type == 'and':
                    sp.z.lb = 1.0
                else:  # sp.parent_combination_type == 'or'
                    sp.z.ub = 0.0
